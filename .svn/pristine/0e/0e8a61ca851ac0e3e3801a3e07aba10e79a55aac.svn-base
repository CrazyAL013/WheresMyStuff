package forms;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Menu;

/**
 * Application Order Form UI
 * The UI code for the Order form (superclass)
 * This class contains code to create and display UI components
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 *
 */
public class OrderFormUI {

	// Control declarations (these controls are available to inheritors)
	protected Shell shell;
	protected Display display;
	protected Text orderNumberTextBox;
	protected Table orderItemsTable;
	protected Text totalTextBox;
	protected Text dateTextBox;
	protected Text text;
	protected Text notesTextBox;
	protected ComboViewer vendorComboViewer;
	protected Combo vendorComboBox;
	protected Combo orderStatusComboBox;
	protected ComboViewer orderStatusComboViewer;
	protected Button okButton;
	protected Button cancelButton;
	protected Text nameTextBox;
	protected Menu orderItemsTablePopupMenu;
	protected Label statusLabel;
	protected TableColumn itemNameTableColumn;
	protected TableColumn stockQuantityTableColumn;
	protected TableColumn quantityTableColumn;

	public OrderFormUI() {
		display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
	}
	
	/**
	 * Create the window and its contents (controls) 
	 */
	protected void createContents() {
		shell = new Shell(display, SWT.APPLICATION_MODAL | SWT.DIALOG_TRIM | SWT.RESIZE);
		shell.setMinimumSize(new Point(480, 500));
		shell.setSize(480, 500);
		shell.setText("Order Form");
		GridLayout gl_shell = new GridLayout(7, false);
		gl_shell.marginTop = 10;
		gl_shell.marginRight = 10;
		gl_shell.marginLeft = 10;
		gl_shell.marginBottom = 10;
		shell.setLayout(gl_shell);
		
		Label nameLabel = new Label(this.shell, SWT.NONE);
		nameLabel.setText("Name:");
		
		this.nameTextBox = new Text(this.shell, SWT.BORDER);
		this.nameTextBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 5, 1));
		
		Composite composite_2 = new Composite(this.shell, SWT.NONE);
		GridData gd_composite_2 = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_composite_2.heightHint = 11;
		composite_2.setLayoutData(gd_composite_2);
		
		Label dateLabel = new Label(shell, SWT.NONE);
		dateLabel.setText("Date:");
		
		dateTextBox = new Text(shell, SWT.BORDER);
		GridData gd_dateTextBox = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_dateTextBox.widthHint = 80;
		gd_dateTextBox.minimumWidth = 80;
		dateTextBox.setLayoutData(gd_dateTextBox);
		
		Label orderNumberLabel = new Label(shell, SWT.NONE);
		orderNumberLabel.setText("Order #:");
		
		orderNumberTextBox = new Text(shell, SWT.BORDER);
		GridData gd_orderNumberTextBox = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_orderNumberTextBox.minimumWidth = 80;
		gd_orderNumberTextBox.widthHint = 80;
		orderNumberTextBox.setLayoutData(gd_orderNumberTextBox);
		
		Label totalLabel = new Label(shell, SWT.NONE);
		totalLabel.setText("Total $:");
		
		totalTextBox = new Text(shell, SWT.BORDER | SWT.RIGHT);
		GridData gd_totalTextBox = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_totalTextBox.minimumWidth = 50;
		gd_totalTextBox.widthHint = 50;
		totalTextBox.setLayoutData(gd_totalTextBox);
		
		Composite composite = new Composite(shell, SWT.NONE);
		GridData gd_composite = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_composite.heightHint = 11;
		composite.setLayoutData(gd_composite);
		
		Label vendorLabel = new Label(shell, SWT.NONE);
		vendorLabel.setText("Vendor:");
		
		vendorComboViewer = new ComboViewer(shell, SWT.READ_ONLY);
		vendorComboBox = vendorComboViewer.getCombo();
		vendorComboBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));
		
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		
		this.statusLabel = new Label(this.shell, SWT.READ_ONLY);
		this.statusLabel.setText("Status:");
		
		orderStatusComboViewer = new ComboViewer(shell, SWT.READ_ONLY);
		orderStatusComboBox = orderStatusComboViewer.getCombo();
		orderStatusComboBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));
		
		new Label(this.shell, SWT.NONE);
		new Label(this.shell, SWT.NONE);
		new Label(this.shell, SWT.NONE);
		
		Label notesLabel = new Label(shell, SWT.NONE);
		GridData gd_notesLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_notesLabel.verticalIndent = 10;
		notesLabel.setLayoutData(gd_notesLabel);
		notesLabel.setText("Notes:");
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		
		notesTextBox = new Text(shell, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		GridData gd_notesTextBox = new GridData(SWT.FILL, SWT.CENTER, true, false, 7, 1);
		gd_notesTextBox.heightHint = 53;
		notesTextBox.setLayoutData(gd_notesTextBox);
		
		orderItemsTable = new Table(shell, SWT.BORDER | SWT.FULL_SELECTION);
		GridData gd_orderItemsTable = new GridData(SWT.FILL, SWT.FILL, true, true, 7, 1);
		gd_orderItemsTable.verticalIndent = 15;
		gd_orderItemsTable.heightHint = 250;
		orderItemsTable.setLayoutData(gd_orderItemsTable);
		orderItemsTable.setHeaderVisible(true);
		orderItemsTable.setLinesVisible(true);
		
		itemNameTableColumn = new TableColumn(orderItemsTable, SWT.NONE);
		itemNameTableColumn.setWidth(268);
		itemNameTableColumn.setText("Item Name");
		itemNameTableColumn.setData(String.class);
		
		stockQuantityTableColumn = new TableColumn(orderItemsTable, SWT.RIGHT);
		stockQuantityTableColumn.setWidth(75);
		stockQuantityTableColumn.setText("Stock Qty");
		stockQuantityTableColumn.setData(Integer.class);
		
		quantityTableColumn = new TableColumn(orderItemsTable, SWT.RIGHT);
		quantityTableColumn.setWidth(75);
		quantityTableColumn.setText("Order Qty");
		quantityTableColumn.setData(Integer.class);
		
		this.orderItemsTablePopupMenu = new Menu(this.orderItemsTable);
		this.orderItemsTable.setMenu(this.orderItemsTablePopupMenu);
		
		Composite composite_1 = new Composite(shell, SWT.NONE);
		GridLayout gl_composite_1 = new GridLayout(2, false);
		gl_composite_1.marginTop = 15;
		gl_composite_1.marginWidth = 0;
		gl_composite_1.verticalSpacing = 0;
		gl_composite_1.marginHeight = 0;
		composite_1.setLayout(gl_composite_1);
		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 7, 1));
			
		okButton = new Button(composite_1, SWT.CENTER);
		GridData gd_okButton = new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1);
		gd_okButton.minimumWidth = 68;
		gd_okButton.widthHint = 68;
		gd_okButton.heightHint = 28;
		gd_okButton.minimumHeight = 28;
		okButton.setLayoutData(gd_okButton);
		okButton.setText("OK");
		
		cancelButton = new Button(composite_1, SWT.NONE);
		GridData gd_cancelButton = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gd_cancelButton.heightHint = 28;
		gd_cancelButton.widthHint = 68;
		gd_cancelButton.minimumHeight = 28;
		gd_cancelButton.minimumWidth = 68;
		cancelButton.setLayoutData(gd_cancelButton);
		cancelButton.setText("Cancel");
	}
}
