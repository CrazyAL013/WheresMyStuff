package global;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

/**
 * Global error handler
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 * 
 */
public final class Errors {
	
	/**
	 * Displays a standard error message box
	 * 
	 * @param shell: The display (SWT) shell
	 * @param title: The title of the message box window
	 * @param message: The message text to be displayed
	 * @param icon: The icon (SWT.ICON) to be displayed
	 */
	public static void display(Shell shell, String title,String message, int icon) {
		MessageBox messageBox = new MessageBox(shell, icon | SWT.OK);
		messageBox.setMessage(message);
		messageBox.setText(title);
		messageBox.open();
	}

}
