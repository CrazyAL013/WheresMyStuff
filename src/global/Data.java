package global;

import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Queue;

import model.Collection;
import model.Group;
import model.HashSet;
import model.Order;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import application.Program;

/**
 * Define global data structures that will be used throughout the application
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 * 
 */
public final class Data {
	public static Properties config = null;				// Application properties
	public static HashMap<String,Image> images = null;	// Application image resources
	public static HashSet stock = null;
	public static HashSet orders = null;
	public static HashSet collections = null;
	public static HashSet vendors = null;
	public static HashSet orderStatuses = null;

	public static class DeletedFilePointers {
		public static Queue<Long> collection = null;
		public static Queue<Long> group = null;
		public static Queue<Long> category = null;
		public static Queue<Long> item = null;
		public static Queue<Long> order = null;
		public static Queue<Long> orderItems = null;
		public static Queue<Long> vendors = null;
		public static Queue<Long> orderStatuses = null;
	}
	
	static {
		Data.loadApplicationResources();
	}

	/**
	 * Loads the application resources (images/icons etc.)
	 */
	public static void loadApplicationResources() {
		// Load the application image resources
		Data.images = new HashMap<String, Image>();
		Data.images.put("FolderClosed", getImage("FolderClosed.png"));
		Data.images.put("FolderOpen", getImage("FolderOpen.png"));
		Data.images.put("AlertRed", getImage("AlertRed.png"));
		Data.images.put("AlertBlue", getImage("AlertBlue.png"));
		Data.images.put("New", getImage("New.png"));
		Data.images.put("Cancelled", getImage("Cancelled.png"));
		Data.images.put("Delivered", getImage("Delivered.png"));
		Data.images.put("Completed", getImage("Completed.png"));
		Data.images.put("Waiting", getImage("Waiting.png"));
		Data.images.put("Shipped", getImage("Shipped.png"));
		Data.images.put("Problem", getImage("Problem.png"));
		Data.images.put("AppIcon16", getImage("wms-icon-16.png"));
		Data.images.put("AppIcon24", getImage("wms-icon-24.png"));
		Data.images.put("AppIcon32", getImage("wms-icon-32.png"));
		Data.images.put("AppIcon48", getImage("wms-icon-48.png"));
		Data.images.put("SplashScreen", getImage("WMS-SplashScreen-500x250.jpg"));
	}
	
	/**
	 * Saves the hashset sizes to the application configuration file
	 */
	public static void saveHashsetSizes() {
		if (Data.stock.count() > 0) Data.config.setProperty("StockCount", Long.toString(Data.stock.count()));
		if (Data.orders.count() > 0) Data.config.setProperty("OrdersCount", Long.toString(Data.orders.count()));
		if (Data.collections.count() > 0) Data.config.setProperty("CollectionsCount", Long.toString(Data.collections.count()));
		if (Data.vendors.count() > 0) Data.config.setProperty("VendorsCount", Long.toString(Data.vendors.count()));
		if (Data.orderStatuses.count() > 0) Data.config.setProperty("orderStatusesCount", Long.toString(Data.orderStatuses.count()));
		
		int orderItemsCount = 0;
		Iterator orders = Data.orders.iterator();
		while (orders.hasNext()) {
			Order order = (Order)orders.next();
			if (order.orderItems.count() > 0) Data.config.setProperty(order.getId().toString(), Long.toString(order.orderItems.count()));
			orderItemsCount++;
		}
		
		int groupsCount = 0;
		int categoriesCount = 0;
		Iterator collections = Data.collections.iterator();
		while (collections.hasNext()) {
			Collection collection = (Collection)collections.next();
			if (collection.groups.count() > 0) Data.config.setProperty(collection.getId().toString(), Long.toString(collection.groups.count()));
			groupsCount += collection.groups.count();
						
			Iterator groups = collection.groups.iterator();
			while (groups.hasNext()) {
				Group group = (Group)groups.next();
				if (group.categories.count() > 0) Data.config.setProperty(group.getId().toString(), Long.toString(group.categories.count()));
				categoriesCount += group.categories.count();
			}
		}
		
		if (orderItemsCount > 0) Data.config.setProperty("OrderItemsCount", Long.toString(orderItemsCount));
		if (groupsCount > 0) Data.config.setProperty("GroupsCount", Long.toString(groupsCount));
		if (categoriesCount > 0) Data.config.setProperty("CategoriesCount", Long.toString(categoriesCount));
		
		try {
			Data.config.store(new FileOutputStream("config.wms"), null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Loads an image file from disk
	 * 
	 * @param file: A String containing the path and name of an image file
	 * @return: An image
	 */
	private static Image getImage(String file) {
		return (new Image(Display.getCurrent(), Program.class.getResourceAsStream("../resources/" + file)));
	}

}