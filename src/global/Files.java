package global;
import java.io.RandomAccessFile;

/**
 * Global File Pointers
 * Define global file pointers that will be used throughout the application
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 *
 */
public final class Files {
	public static RandomAccessFile stock = null;
	public static RandomAccessFile orders = null;
	public static RandomAccessFile orderItems = null;
	public static RandomAccessFile collections = null;
	public static RandomAccessFile groups = null;
	public static RandomAccessFile categories = null;
	public static RandomAccessFile vendors = null;
	public static RandomAccessFile orderStatuses = null;
}
