package global;

import java.text.SimpleDateFormat;
import java.util.UUID;

/**
 * Global constants
 * Define global constants that will be used throughout the application
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 *
 */
public final class Constants {
	// This class cannot be instantiated
    private Constants() { throw new AssertionError(); }
    
    public static final UUID UUID_EMPTY = UUID.fromString("00000000-0000-0000-0000-000000000000"); 
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MMM-yyyy");
}
