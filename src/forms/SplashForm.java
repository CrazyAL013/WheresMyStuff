package forms;

import static global.Constants.UUID_EMPTY;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Queue;

import model.Category;
import model.Collection;
import model.Group;
import model.HashSet;
import model.Item;
import model.ObjectBase;
import model.Order;
import model.OrderItem;
import model.OrderStatus;
import model.Vendor;

import org.eclipse.swt.SWT;

import global.Data;
import global.Errors;
import global.Files;

/**
 * Application Splash form.
 * Displayed while the application starts and data files are loaded.
 * This class contains code to implement the UI functionality.
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 * 
 */
public class SplashForm extends SplashFormUI {
	private HashSet groups;
	private HashSet categories;
	private HashSet orderItems;

	public SplashForm() {

		InitializeApplication();

		// Add a small delay to make sure the splash form remains visible
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Performs all the application initialization functions
	 */
	private void InitializeApplication() {
		// Load application data
		try {
			// Load the application configuration data (from file)
			Data.config = new Properties();
			Data.config.load(new FileInputStream("config.wms"));

			// Create/open the data files
			Files.stock = new RandomAccessFile("items.wms", "rw");
			Files.orders = new RandomAccessFile("orders.wms", "rw");
			Files.orderItems = new RandomAccessFile("orderItems.wms", "rw");
			Files.collections = new RandomAccessFile("collections.wms", "rw");
			Files.groups = new RandomAccessFile("groups.wms", "rw");
			Files.categories = new RandomAccessFile("categories.wms", "rw");
			Files.vendors = new RandomAccessFile("vendors.wms", "rw");
			Files.orderStatuses = new RandomAccessFile("orderStatuses.wms", "rw");

		} catch (FileNotFoundException e) {
			Errors.display(shell, "File Error", "One or more application data files could not be found.", SWT.ICON_ERROR);
		} catch (IOException e) {
			Errors.display(shell, "File Error", "Unable to read one or more application data files.", SWT.ICON_ERROR);
		}

		// Initialize the data hashsets
		Data.stock = new HashSet(Integer.parseInt(Data.config.getProperty("StockCount", "100")));
		Data.orders = new HashSet(Integer.parseInt(Data.config.getProperty("OrdersCount", "50")));
		Data.collections = new HashSet(Integer.parseInt(Data.config.getProperty("CollectionsCount", "100")));
		orderItems = new HashSet(Integer.parseInt(Data.config.getProperty("OrderItemsCount", "500")));
		groups = new HashSet(Integer.parseInt(Data.config.getProperty("GroupsCount", "250")));
		categories = new HashSet(Integer.parseInt(Data.config.getProperty("CategoriesCount", "500")));
		Data.vendors = new HashSet(Integer.parseInt(Data.config.getProperty("VendorsCount", "50")));
		Data.orderStatuses = new HashSet(Integer.parseInt(Data.config.getProperty("StatusesCount", "20")));

		// Initialize the linked lists that will store all deleted record pointers
		Data.DeletedFilePointers.collection = new LinkedList<Long>();
		Data.DeletedFilePointers.group = new LinkedList<Long>();
		Data.DeletedFilePointers.category = new LinkedList<Long>();
		Data.DeletedFilePointers.item = new LinkedList<Long>();
		Data.DeletedFilePointers.order = new LinkedList<Long>();
		Data.DeletedFilePointers.orderItems = new LinkedList<Long>();
		Data.DeletedFilePointers.vendors = new LinkedList<Long>();
		Data.DeletedFilePointers.orderStatuses = new LinkedList<Long>();

		// Load the data
		try {
			loadCollections(Files.collections, Data.collections, Data.DeletedFilePointers.collection);
			loadGroups(Files.groups, this.groups, Data.DeletedFilePointers.group);
			loadCategories(Files.categories, this.categories, Data.DeletedFilePointers.category);
			loadVendors(Files.vendors, Data.vendors, Data.DeletedFilePointers.vendors);
			loadStatuses(Files.orderStatuses, Data.orderStatuses, Data.DeletedFilePointers.orderStatuses);
			loadStock(Files.stock, Data.stock, Data.DeletedFilePointers.item);
			loadOrders(Files.orders, Data.orders, Data.DeletedFilePointers.order);
			loadOrderItems(Files.orderItems, this.orderItems, Data.DeletedFilePointers.orderItems);
		} catch (IOException e1) {
			Errors.display(shell, "File Error", "Failed to application data.\n\nPlease check application data files are present.", SWT.ICON_ERROR);
		} catch (ParseException e) {
			Errors.display(shell, "File Error", "Failed to load applicatioon data.\n\nThe data files may be corrupt.", SWT.ICON_ERROR);
		}

	}

	/**
	 * Loads the stock items into a hashset
	 * @param file: An open file handle (to read from)
	 * @param set: The hashet to populate
	 */
	private void loadStock(RandomAccessFile file, HashSet dataSet, Queue<Long> deletedRecordSet) throws IOException, ParseException {
		while (file.getFilePointer() < file.length()) {
			Item data = new Item(file, file.getFilePointer());
			loadData(data, dataSet, deletedRecordSet);
		}
	}

	/**
	 * Loads the orders into a hashset
	 * @param file: An open file handle (to read from)
	 * @param set: The hashet to populate
	 */
	private void loadOrders(RandomAccessFile file, HashSet dataSet, Queue<Long> deletedRecordSet) throws IOException, ParseException {
		while (file.getFilePointer() < file.length()) {
			Order data = new Order(file, file.getFilePointer());
			loadData(data, dataSet, deletedRecordSet);
		}
	}

	/**
	 * Loads the order items into a hashset
	 * @param file: An open file handle (to read from)
	 * @param set: The hashet to populate
	 */
	private void loadOrderItems(RandomAccessFile file, HashSet dataSet, Queue<Long> deletedRecordSet) throws IOException, ParseException {
		while (file.getFilePointer() < file.length()) {
			OrderItem data = new OrderItem(file, file.getFilePointer());
			if (loadData(data, dataSet, deletedRecordSet)) {
				// OrderItems are contained within a parent Order: locate the Order and add the OrderItem
				((Order) Data.orders.get(data.getOrderId())).orderItems.add(data.getId(), data);
			}
		}
	}

	/**
	 * Loads the collections into a hashset
	 * @param file: An open file handle (to read from)
	 * @param set: The hashet to populate
	 */
	private void loadCollections(RandomAccessFile file, HashSet dataSet, Queue<Long> deletedRecordSet) throws IOException, ParseException {
		while (file.getFilePointer() < file.length()) {
			Collection data = new Collection(file, file.getFilePointer());
			loadData(data, dataSet, deletedRecordSet);
		}
	}

	/**
	 * Loads the groups into a hashset
	 * @param file: An open file handle (to read from)
	 * @param set: The hashet to populate
	 */
	private void loadGroups(RandomAccessFile file, HashSet dataSet, Queue<Long> deletedRecordSet) throws IOException, ParseException {
		while (file.getFilePointer() < file.length()) {
			Group data = new Group(file, file.getFilePointer());
			if (loadData(data, dataSet, deletedRecordSet)) {
				// Groups are contained within a parent Collection: locate the Collection and add the Group
				((Collection) Data.collections.get(data.getCollectionID())).groups.add(data.getId(), data);
			}
		}
	}

	/**
	 * Loads the categories into a hashset
	 * @param file: An open file handle (to read from)
	 * @param set: The hashet to populate
	 */
	private void loadCategories(RandomAccessFile file, HashSet dataSet, Queue<Long> deletedRecordSet) throws IOException, ParseException {
		while (file.getFilePointer() < file.length()) {
			Category data = new Category(file, file.getFilePointer());
			if (loadData(data, dataSet, deletedRecordSet)) {
				// Categories are contained within a parent Group: locate the Group and add the Category
				((Group) groups.get(data.getGroupID())).categories.add(data.getId(), data);
			}
		}
	}

	/**
	 * Loads the vendors into a hashset
	 * @param file: An open file handle (to read from)
	 * @param set: The hashet to populate
	 */
	private void loadVendors(RandomAccessFile file, HashSet dataSet, Queue<Long> deletedRecordSet) throws IOException, ParseException {
		while (file.getFilePointer() < file.length()) {
			Vendor data = new Vendor(file, file.getFilePointer());
			loadData(data, dataSet, deletedRecordSet);
		}
	}

	/**
	 * Loads the statuses into a hashset
	 * @param file: An open file handle (to read from)
	 * @param set: The hashet to populate
	 */
	private void loadStatuses(RandomAccessFile file, HashSet dataSet, Queue<Long> deletedRecordSet) throws IOException, ParseException {
		while (file.getFilePointer() < file.length()) {
			OrderStatus data = new OrderStatus(file, file.getFilePointer());
			loadData(data, dataSet, deletedRecordSet);
		}
	}

	/**
	 * Loads an object into a hashset or saves a deleted record pointer
	 * @param data: A data object to be stored
	 * @param dataSet: A hashset used to store data objects
	 * @param deletedRecordSet: A queue used to store record pointers of deleted file records
	 * @return: A flag to indicate whether the object was loaded (true) or not
	 */
	private boolean loadData(ObjectBase data, HashSet dataSet, Queue<Long> deletedRecordSet) throws IOException, ParseException {
		// Call the read method on the ObjectBase superclass (polymorphic)
		data.read();

		// Check for an empty Id ("00000000-0000-0000-0000-000000000000")
		if (!data.getId().equals(UUID_EMPTY)) {
			// Valid Id found: add the data object to the hashset
			dataSet.add(data.getId(), data);
			return (true);
		} else {
			// Empty Id found: add the record pointer to deleted records queue
			deletedRecordSet.add(data.getRecordPointer());
			return (false);
		}
	}
}