package forms;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

/**
 * Application Stock Item Form UI
 * The UI code for the Stock Item form (superclass)
 * This class contains code to create and display UI components
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 *
 */
public class StockItemFormUI {

	// Control declarations (these controls are available to inheritors)
	protected Shell shell;
	protected Display display;
	protected Text nameTextBox;
	protected Text reOrderLevelTextBox;
	protected Text stockQuantityTextBox;
	protected Text descriptionTextBox;
	protected Text locationTextBox;
	protected Button okButton;
	protected Button cancelButton;
	protected Combo collectionComboBox;
	protected ComboViewer collectionComboViewer;
	protected Combo groupComboBox;
	protected ComboViewer groupComboViewer;
	protected Combo categoryComboBox;
	protected ComboViewer categoryComboViewer;

	public StockItemFormUI() {
		display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
	}

	/**
	 * Create the window and its contents (controls) 
	 */
	protected void createContents() {
		shell = new Shell(display, SWT.APPLICATION_MODAL | SWT.DIALOG_TRIM);
		shell.setMinimumSize(new Point(450, 450));
		shell.setSize(450, 450);
		GridLayout gl_shell = new GridLayout(5, false);
		gl_shell.marginRight = 10;
		gl_shell.marginLeft = 10;
		gl_shell.marginBottom = 10;
		gl_shell.marginTop = 10;
		shell.setLayout(gl_shell);
		
		Label nameLabel = new Label(shell, SWT.NONE);
		nameLabel.setText("Item Name:");
		
		nameTextBox = new Text(shell, SWT.BORDER);
		nameTextBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
		
		Label lblQuantity = new Label(shell, SWT.NONE);
		lblQuantity.setText("Stock Quantity:");
		
		stockQuantityTextBox = new Text(shell, SWT.BORDER | SWT.RIGHT);
		GridData gd_stockQuantityTextBox = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_stockQuantityTextBox.widthHint = 50;
		gd_stockQuantityTextBox.minimumWidth = 50;
		stockQuantityTextBox.setLayoutData(gd_stockQuantityTextBox);
		
		Label collectionLabel = new Label(shell, SWT.NONE);
		collectionLabel.setText("Collection:");
		
		collectionComboViewer = new ComboViewer(shell, SWT.READ_ONLY);
		collectionComboBox = collectionComboViewer.getCombo();
		collectionComboBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		
		Label reorderLevelLabel = new Label(shell, SWT.NONE);
		reorderLevelLabel.setText("Re-Order Level:");
		
		reOrderLevelTextBox = new Text(shell, SWT.BORDER | SWT.RIGHT);
		GridData gd_reOrderLevelTextBox = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_reOrderLevelTextBox.minimumWidth = 50;
		gd_reOrderLevelTextBox.widthHint = 50;
		reOrderLevelTextBox.setLayoutData(gd_reOrderLevelTextBox);
		
		Label groupLabel = new Label(shell, SWT.NONE);
		groupLabel.setText("Group:");
		
		groupComboViewer = new ComboViewer(shell, SWT.READ_ONLY);
		groupComboBox = groupComboViewer.getCombo();
		groupComboBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		
		Label categoryLabel = new Label(shell, SWT.NONE);
		categoryLabel.setText("Category:");
		
		categoryComboViewer = new ComboViewer(shell, SWT.READ_ONLY);
		categoryComboBox = categoryComboViewer.getCombo();
		categoryComboBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		
		Label locationLabel = new Label(shell, SWT.NONE);
		locationLabel.setText("Storage Location:");
		
		locationTextBox = new Text(shell, SWT.BORDER);
		locationTextBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
		
		Label descriptionLabel = new Label(shell, SWT.NONE);
		GridData gd_descriptionLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_descriptionLabel.verticalIndent = 10;
		descriptionLabel.setLayoutData(gd_descriptionLabel);
		descriptionLabel.setText("Description:");
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		
		descriptionTextBox = new Text(shell, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		GridData gd_descriptionTextBox = new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1);
		gd_descriptionTextBox.heightHint = 155;
		descriptionTextBox.setLayoutData(gd_descriptionTextBox);
		
		Composite composite = new Composite(shell, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 5, 1));
		GridLayout gl_composite = new GridLayout(2, false);
		gl_composite.marginTop = 15;
		gl_composite.verticalSpacing = 0;
		gl_composite.marginWidth = 0;
		gl_composite.marginHeight = 0;
		composite.setLayout(gl_composite);
		
		okButton = new Button(composite, SWT.NONE);
		GridData gd_okButton = new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1);
		gd_okButton.heightHint = 28;
		gd_okButton.widthHint = 68;
		gd_okButton.minimumWidth = 68;
		gd_okButton.minimumHeight = 28;
		okButton.setLayoutData(gd_okButton);
		okButton.setText("OK");
		
		cancelButton = new Button(composite, SWT.NONE);
		GridData gd_cancelButton = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gd_cancelButton.heightHint = 28;
		gd_cancelButton.minimumHeight = 28;
		gd_cancelButton.minimumWidth = 68;
		gd_cancelButton.widthHint = 68;
		cancelButton.setLayoutData(gd_cancelButton);
		cancelButton.setText("Cancel");
	}
}
