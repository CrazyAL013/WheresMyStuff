package forms;

import java.util.Date;

import global.Data;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.Button;

/**
 * Application Main Form UI
 * The UI code for the About form (superclass)
 * This class contains code to create and display UI components
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 *
 */
public class MainFormUI {

	// Control declarations (these controls are available to inheritors)
	protected Shell shell;
	protected Display display;
	protected ToolBar mainToolbar;
	protected Table stockTabTable;
	protected Text searchStockTextBox;
	protected Text searchOrdersTextBox;
	protected Table ordersTabTable;
	protected TabItem stockTabItem;
	protected TabItem ordersTabItem;
	protected Tree stockTabTree;
	protected MenuItem exitMenuItem;
	protected MenuItem helpMenuItem;
	protected Label statusBarLabel1;
	protected Label statusBarLabel2;
	protected TabFolder mainTabFolder;
	protected Menu stockTabTablePopupMenu;
	protected Menu stockTabTreePopupMenu;
	protected Menu ordersTabTablePopupMenu;
	protected TableColumn itemNameTableColumn;
	protected TableColumn itemStockQuantityTableColumn;
	protected TableColumn orderNameTableColumn;
	protected MenuItem exportStockMenuItem;
	protected MenuItem importStockMenuItem;
	protected TableColumn orderIconTableColumn;
	protected TableColumn orderDateTableColumn;
	protected TableColumn orderRefTableColumn;
	protected TableColumn orderTotalTableColumn;
	protected TableColumn stockItemIconTableColumn;
	protected Button searchStockClearButton;
	protected Button searchOrdersClearButton;
	
	public MainFormUI() {
		display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
	}
	
	/**
	 * Create the window and its contents (controls) 
	 */
	protected void createContents() {
		shell = new Shell(display);
		shell.setMinimumSize(new Point(700, 600));
		shell.setSize(700, 600);
		shell.setImages(new Image[] { Data.images.get("AppIcon16"), Data.images.get("AppIcon24"), Data.images.get("AppIcon32"), Data.images.get("AppIcon48") });

		Rectangle bounds = display.getPrimaryMonitor().getBounds();
		Rectangle rect = shell.getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		shell.setLocation(x, y);

		GridLayout gl_shell = new GridLayout(1, false);
		shell.setLayout(gl_shell);

		Menu mainMenu = new Menu(shell, SWT.BAR);
		shell.setMenuBar(mainMenu);

		MenuItem fileMenuHeader = new MenuItem(mainMenu, SWT.CASCADE);
		fileMenuHeader.setText("&File");

		Menu fileMenu = new Menu(fileMenuHeader);
		fileMenuHeader.setMenu(fileMenu);

		new MenuItem(fileMenu, SWT.SEPARATOR);

		this.exportStockMenuItem = new MenuItem(fileMenu, SWT.NONE);
		this.exportStockMenuItem.setText("Export Stock...");

		this.importStockMenuItem = new MenuItem(fileMenu, SWT.NONE);
		this.importStockMenuItem.setText("Import Stock...");
		
		new MenuItem(fileMenu, SWT.SEPARATOR);

		exitMenuItem = new MenuItem(fileMenu, SWT.NONE);
		exitMenuItem.setText("E&xit");

		MenuItem helpMenuHeader = new MenuItem(mainMenu, SWT.CASCADE);
		helpMenuHeader.setText("&Help");

		Menu helpMenu = new Menu(helpMenuHeader);
		helpMenuHeader.setMenu(helpMenu);

		helpMenuItem = new MenuItem(helpMenu, SWT.NONE);
		helpMenuItem.setText("About");

		mainToolbar = new ToolBar(shell, SWT.FLAT | SWT.RIGHT);

		Composite mainFormComposite = new Composite(shell, SWT.NONE);
		GridLayout gl_mainFormComposite = new GridLayout(1, false);
		gl_mainFormComposite.verticalSpacing = 0;
		gl_mainFormComposite.marginHeight = 0;
		gl_mainFormComposite.marginWidth = 0;
		gl_mainFormComposite.marginTop = 10;
		gl_mainFormComposite.marginRight = 10;
		gl_mainFormComposite.marginLeft = 10;
		mainFormComposite.setLayout(gl_mainFormComposite);
		mainFormComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		mainTabFolder = new TabFolder(mainFormComposite, SWT.NONE);
		mainTabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		stockTabItem = new TabItem(mainTabFolder, SWT.NONE);
		stockTabItem.setText("Stock");

		Composite stockTabComposite = new Composite(mainTabFolder, SWT.NONE);
		stockTabItem.setControl(stockTabComposite);
		GridLayout gl_stockTabComposite = new GridLayout(1, false);
		gl_stockTabComposite.verticalSpacing = 0;
		gl_stockTabComposite.marginWidth = 0;
		gl_stockTabComposite.marginTop = 10;
		gl_stockTabComposite.marginRight = 10;
		gl_stockTabComposite.marginLeft = 10;
		gl_stockTabComposite.marginHeight = 0;
		gl_stockTabComposite.marginBottom = 10;
		stockTabComposite.setLayout(gl_stockTabComposite);

		Composite stockSearchComposite = new Composite(stockTabComposite, SWT.NONE);
		GridLayout gl_stockSearchComposite = new GridLayout(3, false);
		gl_stockSearchComposite.verticalSpacing = 0;
		gl_stockSearchComposite.marginHeight = 0;
		gl_stockSearchComposite.marginWidth = 0;
		gl_stockSearchComposite.marginTop = 10;
		gl_stockSearchComposite.marginBottom = 10;
		stockSearchComposite.setLayout(gl_stockSearchComposite);
		GridData gd_stockSearchComposite = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_stockSearchComposite.heightHint = 47;
		stockSearchComposite.setLayoutData(gd_stockSearchComposite);

		searchStockTextBox = new Text(stockSearchComposite, SWT.BORDER);
		GridData gd_searchStockTextBox = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_searchStockTextBox.widthHint = 223;
		searchStockTextBox.setLayoutData(gd_searchStockTextBox);
		
		this.searchStockClearButton = new Button(stockSearchComposite, SWT.NONE);
		this.searchStockClearButton.setText("&Clear");
		new Label(stockSearchComposite, SWT.NONE);

		SashForm stockTabSashForm = new SashForm(stockTabComposite, SWT.NONE);
		stockTabSashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		stockTabTree = new Tree(stockTabSashForm, SWT.BORDER);

		stockTabTable = new Table(stockTabSashForm, SWT.BORDER | SWT.FULL_SELECTION);

		stockTabTable.setHeaderVisible(true);
		stockTabTable.setLinesVisible(true);

		stockItemIconTableColumn = new TableColumn(stockTabTable, SWT.RIGHT);
		this.stockItemIconTableColumn.setResizable(false);
		stockItemIconTableColumn.setWidth(20);
		stockItemIconTableColumn.setData(String.class);

		itemNameTableColumn = new TableColumn(stockTabTable, SWT.NONE);
		itemNameTableColumn.setWidth(332);
		itemNameTableColumn.setText("Name");
		itemNameTableColumn.setData(String.class);

		itemStockQuantityTableColumn = new TableColumn(stockTabTable, SWT.RIGHT);
		itemStockQuantityTableColumn.setWidth(55);
		itemStockQuantityTableColumn.setText("Qty");
		itemStockQuantityTableColumn.setData(Integer.class);

		ordersTabItem = new TabItem(mainTabFolder, SWT.NONE);
		ordersTabItem.setText("Orders");

		Composite ordersTabComposite = new Composite(mainTabFolder, SWT.NONE);
		ordersTabItem.setControl(ordersTabComposite);
		GridLayout gl_ordersTabComposite = new GridLayout(1, false);
		gl_ordersTabComposite.marginTop = 10;
		gl_ordersTabComposite.marginRight = 10;
		gl_ordersTabComposite.marginLeft = 10;
		gl_ordersTabComposite.marginBottom = 10;
		gl_ordersTabComposite.marginWidth = 0;
		gl_ordersTabComposite.marginHeight = 0;
		ordersTabComposite.setLayout(gl_ordersTabComposite);

		Composite ordersSearchComposite = new Composite(ordersTabComposite, SWT.NONE);
		GridLayout gl_ordersSearchComposite = new GridLayout(3, false);
		gl_ordersSearchComposite.marginTop = 10;
		gl_ordersSearchComposite.marginBottom = 10;
		gl_ordersSearchComposite.marginHeight = 0;
		gl_ordersSearchComposite.marginWidth = 0;
		ordersSearchComposite.setLayout(gl_ordersSearchComposite);
		ordersSearchComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		searchOrdersTextBox = new Text(ordersSearchComposite, SWT.BORDER);
		searchOrdersTextBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		this.searchOrdersClearButton = new Button(ordersSearchComposite, SWT.NONE);
		this.searchOrdersClearButton.setText("&Clear");
		new Label(ordersSearchComposite, SWT.NONE);

		ordersTabTable = new Table(ordersTabComposite, SWT.BORDER | SWT.FULL_SELECTION);
		ordersTabTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		ordersTabTable.setHeaderVisible(true);
		ordersTabTable.setLinesVisible(true);

		orderIconTableColumn = new TableColumn(ordersTabTable, SWT.NONE);
		this.orderIconTableColumn.setResizable(false);
		orderIconTableColumn.setWidth(20);
		orderIconTableColumn.setData(String.class);

		orderNameTableColumn = new TableColumn(ordersTabTable, SWT.NONE);
		orderNameTableColumn.setWidth(320);
		orderNameTableColumn.setText("Name");
		orderNameTableColumn.setData(String.class);

		orderDateTableColumn = new TableColumn(ordersTabTable, SWT.NONE);
		orderDateTableColumn.setWidth(101);
		orderDateTableColumn.setText("Date");
		orderDateTableColumn.setData(Date.class);
		
		orderRefTableColumn = new TableColumn(ordersTabTable, SWT.NONE);
		orderRefTableColumn.setWidth(90);
		orderRefTableColumn.setText("Ref");
		orderRefTableColumn.setData(String.class);
		
		orderTotalTableColumn = new TableColumn(ordersTabTable, SWT.RIGHT);
		orderTotalTableColumn.setWidth(77);
		orderTotalTableColumn.setText("Total $");
		orderTotalTableColumn.setData(Float.class);
		
		stockTabSashForm.setWeights(new int[] { 217, 428 });

		Composite statusBarComposite = new Composite(mainFormComposite, SWT.BORDER);
		statusBarComposite.setLayout(new GridLayout(2, false));
		GridData gd_statusBarComposite = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_statusBarComposite.heightHint = 28;
		gd_statusBarComposite.verticalIndent = 10;
		statusBarComposite.setLayoutData(gd_statusBarComposite);

		statusBarLabel1 = new Label(statusBarComposite, SWT.NONE);
		GridData gd_statusBarLabel1 = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_statusBarLabel1.minimumWidth = -1;
		statusBarLabel1.setLayoutData(gd_statusBarLabel1);

		statusBarLabel2 = new Label(statusBarComposite, SWT.RIGHT);
		this.statusBarLabel2.setAlignment(SWT.RIGHT);
		GridData gd_statusBarLabel2 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_statusBarLabel2.widthHint = 150;
		gd_statusBarLabel2.minimumWidth = -1;
		statusBarLabel2.setLayoutData(gd_statusBarLabel2);

		this.stockTabTablePopupMenu = new Menu(this.stockTabTable);
		this.stockTabTable.setMenu(stockTabTablePopupMenu);

		this.ordersTabTablePopupMenu = new Menu(this.ordersTabTable);
		this.ordersTabTable.setMenu(this.ordersTabTablePopupMenu);

		this.stockTabTreePopupMenu = new Menu(this.stockTabTree);
		this.stockTabTree.setMenu(this.stockTabTreePopupMenu);

	}
} 