package forms;

import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;

/**
 * Application About form
 * Displays information about the application
 * This class contains code to implement the UI functionality
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 *
 */
public class AboutForm extends AboutFormUI {

	public AboutForm() {
		shell.setText("About Where's My Stuff?");
		
		// Add UI listeners
		shell.addMouseListener(formMouseAction);
	
		// Wait for user input
		while (!this.shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
	
	/**
	 * Handle mouse events on the form
	 */
	MouseAdapter formMouseAction = new MouseAdapter() {
		public void mouseDown(MouseEvent e) {
			// Clicking anywhere on the form will close it
			shell.close();
		}
	};
}