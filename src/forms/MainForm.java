package forms;

import static global.Constants.DATE_FORMAT;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.Collator;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import model.Category;
import model.Collection;
import model.Group;
import model.Item;
import model.ObjectBase;
import model.Order;
import model.OrderItem;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TreeEditor;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.events.TreeAdapter;
import org.eclipse.swt.events.TreeEvent;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import global.Data;
import global.Errors;
import global.Files;

/**
 * Application Main form.Displays the Main form.
 * This class contains code to implement the UI functionality.
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 * 
 */
public class MainForm extends MainFormUI {

	public MainForm(SplashForm splashForm) {
		shell.setText("Where's My Stuff?");
		
		// Configure form fields
		stockTabTable.setSortColumn(itemNameTableColumn);
		stockTabTable.setSortDirection(SWT.DOWN);
		ordersTabTable.setSortColumn(orderNameTableColumn);
		ordersTabTable.setSortDirection(SWT.DOWN);

		// Populate the stock tree/stock table/order table
		populateStockTabTree();
		populateOrdersTabTable();

		// Add UI listeners
		shell.addShellListener(formEvent);
		
		exitMenuItem.addSelectionListener(mainMenuItemSelected);
		helpMenuItem.addSelectionListener(mainMenuItemSelected);
		exportStockMenuItem.addSelectionListener(mainMenuItemSelected);
		importStockMenuItem.addSelectionListener(mainMenuItemSelected);

		stockTabTree.addMouseListener(stockTabTreeMouseAction);
		stockTabTree.addSelectionListener(stockTabTreeItemSelected);
		stockTabTree.addKeyListener(stockTabTreeKeyPressed);
		stockTabTree.addTreeListener(stockTabTreeEvent);

		stockTabTable.addMouseListener(stockTabTableMouseAction);
		stockItemIconTableColumn.addSelectionListener(stockTabTableColumnSelectionListener);
		itemNameTableColumn.addSelectionListener(stockTabTableColumnSelectionListener);
		itemStockQuantityTableColumn.addSelectionListener(stockTabTableColumnSelectionListener);

		ordersTabTable.addMouseListener(ordersTabTableMouseAction);
		orderIconTableColumn.addSelectionListener(ordersTabTableColumnSelectionListener);
		orderNameTableColumn.addSelectionListener(ordersTabTableColumnSelectionListener);
		orderRefTableColumn.addSelectionListener(ordersTabTableColumnSelectionListener);
		orderTotalTableColumn.addSelectionListener(ordersTabTableColumnSelectionListener);
		orderDateTableColumn.addSelectionListener(ordersTabTableColumnSelectionListener);

		mainTabFolder.addSelectionListener(mainTabFolderItemChanged);

		searchStockTextBox.addKeyListener(searchStockTextBoxKeyPressed);
		searchOrdersTextBox.addKeyListener(searchOrdersTextBoxKeyPressed);
		
		searchStockClearButton.addSelectionListener(searchStockClearButtonPressed);
		searchOrdersClearButton.addSelectionListener(searchOrdersClearButtonPressed);

		// Select the first item in the tree
		if (stockTabTree.getItemCount() > 0) {
			stockTabTree.setSelection(stockTabTree.getItem(0));
			Event event = new Event();
			event.item = stockTabTree.getItem(0);
			stockTabTree.notifyListeners(SWT.Selection, event);
		}

		// Close the splash form
		splashForm.shell.close();

		// Wait for user input
		while (!this.shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Shell listener: form open/close etc.
	 */
	ShellAdapter formEvent = new ShellAdapter() {
		public void shellActivated(ShellEvent e) {
			//
		}

		public void shellClosed(ShellEvent e) {
			TerminateApplication();
		}

	};
	
	/**
	 * Implement tree node text edit on F2 keypress
	 */
	KeyAdapter stockTabTreeKeyPressed = new KeyAdapter() {
		public void keyPressed(KeyEvent e) {
			if (e.keyCode == SWT.F2) {
				TreeItem selectedItem = ((Tree) e.getSource()).getSelection()[0];
				editTreeNode(selectedItem);
			}
		}
	};

	/**
	 * Implement stock search text box key listener
	 */
	KeyAdapter searchStockTextBoxKeyPressed = new KeyAdapter() {
		public void keyPressed(KeyEvent e) {
			if (e.keyCode == 13) {
				// Pressing the <enter> key triggers a search
				populateStockTabTableBySearch(searchStockTextBox.getText());
			}
		}
	};

	/**
	 * Implement stock tab table column selection listener
	 */
	SelectionAdapter stockTabTableColumnSelectionListener = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			// The table column header was clicked: Sort the table
			TableColumn selectedColumn = (TableColumn) e.widget;
			if (stockTabTable.getSortColumn() == selectedColumn) {
				stockTabTable.setSortDirection((stockTabTable.getSortDirection() == SWT.UP) ? SWT.DOWN : SWT.UP);
			} else {
				stockTabTable.setSortColumn(selectedColumn);
				stockTabTable.setSortDirection(SWT.UP);
			}
			sortTable(stockTabTable, selectedColumn);
		}
	};

	/**
	 * Implement orders tab table column selection listener
	 */
	SelectionAdapter ordersTabTableColumnSelectionListener = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			// The table column header was clicked: Sort the table
			TableColumn selectedColumn = (TableColumn) e.widget;
			if (ordersTabTable.getSortColumn() == selectedColumn) {
				ordersTabTable.setSortDirection((ordersTabTable.getSortDirection() == SWT.UP) ? SWT.DOWN : SWT.UP);
			} else {
				ordersTabTable.setSortColumn(selectedColumn);
				ordersTabTable.setSortDirection(SWT.UP);
			}
			sortTable(ordersTabTable, selectedColumn);
		}
	};

	/**
	 * Implement orders search text box key listener
	 */
	KeyAdapter searchOrdersTextBoxKeyPressed = new KeyAdapter() {
		public void keyPressed(KeyEvent e) {
			if (e.keyCode == 13) {
				// Pressing the <enter> key triggers a search
				populateOrdersTabTableBySearch(searchOrdersTextBox.getText().trim().toLowerCase());
			}
		}
	};

	/**
	 * Handle stock tab table popup menu selections
	 */
	SelectionAdapter stockTabTablePopupMenuItemClicked = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			TableItem tableItem = null;
			if (stockTabTable.getSelectionCount() > 0) {
				tableItem = stockTabTable.getSelection()[0];
			}

			if (((MenuItem) e.getSource()).getData().equals("NewItem")) {
				openStockItem(null);
			} else if (((MenuItem) e.getSource()).getData().equals("OpenItem")) {
				openStockItem(tableItem);
			} else if (((MenuItem) e.getSource()).getData().equals("RemoveItem")) {
				removeStockItem(tableItem);
			} else if (((MenuItem) e.getSource()).getData().equals("AddToOrder")) {
				addToOrder(tableItem);
			}
		}
	};

	/**
	 * Handle stock tab tree events
	 */
	TreeAdapter stockTabTreeEvent = new TreeAdapter() {
		public void treeExpanded(TreeEvent e) {
			((TreeItem) e.item).setImage(Data.images.get("FolderOpen"));
		}

		public void treeCollapsed(TreeEvent e) {
			((TreeItem) e.item).setImage(Data.images.get("FolderClosed"));
		}
	};

	/**
	 * Handle stock tab tree popup menu selections
	 */
	SelectionAdapter stockTabTreePopupMenuItemClicked = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			TreeItem selectedItem = (stockTabTree.getSelectionCount() == 0) ? null : stockTabTree.getSelection()[0];
			TreeItem newTreeItem = null;
			if (((MenuItem) e.getSource()).getData().equals("NewCollection")) {
				newCollection();
			} else if (((MenuItem) e.getSource()).getData().equals("NewGroup")) {
				if (selectedItem.getData() instanceof Group) {
					newGroup(selectedItem.getParentItem());
				} else {
					newGroup(selectedItem);
				}
			} else if (((MenuItem) e.getSource()).getData().equals("NewCategory")) {
				if (selectedItem.getData() instanceof Category) {
					newCategory(selectedItem.getParentItem());
				} else {
					newCategory(selectedItem);
				}
			} else if (((MenuItem) e.getSource()).getData().equals("NewItem")) {
				openStockItem(null);
			} else if (((MenuItem) e.getSource()).getData().equals("Rename")) {
				editTreeNode(selectedItem);
			} else if (((MenuItem) e.getSource()).getData().equals("Delete")) {
				deleteTreeNode(selectedItem);
			}

			if (newTreeItem != null) {
				// A new tree node was added: make sure it is visible and selected
				stockTabTree.showItem(newTreeItem);
				stockTabTree.setSelection(newTreeItem);
			}
		}
	};

	/**
	 * Handle orders tab table popup menu selections
	 */
	SelectionAdapter ordersTabTablePopupMenuItemClicked = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			TableItem tableItem = null;
			if (ordersTabTable.getSelectionCount() > 0) {
				tableItem = ordersTabTable.getSelection()[0];
			}

			if (((MenuItem) e.getSource()).getData().equals("NewOrder")) {
				openOrder(null);
			} else if (((MenuItem) e.getSource()).getData().equals("OpenOrder")) {
				openOrder(tableItem);
			} else if (((MenuItem) e.getSource()).getData().equals("DeleteOrder")) {
				deleteOrder(tableItem);
			} else if (((MenuItem) e.getSource()).getData().equals("UpdateStock")) {
				updateStock(tableItem);
			}
		}
	};

	/**
	 * Handle mouse events on the stock tab tree
	 */
	MouseAdapter stockTabTreeMouseAction = new MouseAdapter() {
		public void mouseDoubleClick(MouseEvent e) {
			if (e.button == 1) {
				((TreeItem) stockTabTree.getItem(new Point(e.x, e.y))).setExpanded(true);
			}
		}

		public void mouseDown(MouseEvent e) {
			// Display popup menu
			if (e.button == 3) {
				TreeItem treeItem = stockTabTree.getItem(stockTabTree.toControl(display.getCursorLocation()));
				clearPopupMenuItems(stockTabTreePopupMenu);
				if (treeItem == null) {
					createMenuItem(stockTabTreePopupMenu, SWT.NONE, "New &Collection", "NewCollection", stockTabTreePopupMenuItemClicked);
				} else {
					stockTabTree.setSelection(treeItem);
					if (treeItem.getData() instanceof Collection) {
						createMenuItem(stockTabTreePopupMenu, SWT.NONE, "New &Collection", "NewCollection", stockTabTreePopupMenuItemClicked);
						createMenuItem(stockTabTreePopupMenu, SWT.NONE, "New &Group", "NewGroup", stockTabTreePopupMenuItemClicked);
					} else if (treeItem.getData() instanceof Group) {
						createMenuItem(stockTabTreePopupMenu, SWT.NONE, "New &Group", "NewGroup", stockTabTreePopupMenuItemClicked);
						createMenuItem(stockTabTreePopupMenu, SWT.NONE, "New Cate&gory", "NewCategory", stockTabTreePopupMenuItemClicked);
					} else if (treeItem.getData() instanceof Category) {
						createMenuItem(stockTabTreePopupMenu, SWT.NONE, "New Cate&gory", "NewCategory", stockTabTreePopupMenuItemClicked);
						createMenuItem(stockTabTreePopupMenu, SWT.NONE, "&New Item", "NewItem", stockTabTreePopupMenuItemClicked);
					}
					new MenuItem(stockTabTreePopupMenu, SWT.SEPARATOR);
					createMenuItem(stockTabTreePopupMenu, SWT.NONE, "Rename...", "Rename", stockTabTreePopupMenuItemClicked);
					new MenuItem(stockTabTreePopupMenu, SWT.SEPARATOR);
					createMenuItem(stockTabTreePopupMenu, SWT.NONE, "Delete", "Delete", stockTabTreePopupMenuItemClicked);
				}
			}
		}
	};

	/**
	 * Handle mouse events on the stock tab table
	 */
	MouseAdapter stockTabTableMouseAction = new MouseAdapter() {
		public void mouseDoubleClick(MouseEvent e) {
			if (e.button == 1) {
				openStockItem(stockTabTable.getItem(new Point(e.x, e.y)));
			}
		}

		public void mouseDown(MouseEvent e) {
			// Display popup menu
			if (e.button == 3) {
				TableItem tableItem = stockTabTable.getItem(stockTabTable.toControl(display.getCursorLocation()));
				clearPopupMenuItems(stockTabTablePopupMenu);
				if (tableItem == null) {
					createMenuItem(stockTabTablePopupMenu, SWT.NONE, "&New Item", "NewItem", stockTabTablePopupMenuItemClicked);
				} else {
					stockTabTable.setSelection(tableItem);
					createMenuItem(stockTabTablePopupMenu, SWT.NONE, "&New Item", "NewItem", stockTabTablePopupMenuItemClicked);
					createMenuItem(stockTabTablePopupMenu, SWT.NONE, "&Open Item", "OpenItem", stockTabTablePopupMenuItemClicked);
					createMenuItem(stockTabTablePopupMenu, SWT.NONE, "&Remove Item", "RemoveItem", stockTabTablePopupMenuItemClicked);
					new MenuItem(stockTabTablePopupMenu, SWT.SEPARATOR);
					createMenuItem(stockTabTablePopupMenu, SWT.NONE, "&Add To Order", "AddToOrder", stockTabTablePopupMenuItemClicked);
				}
			}
		}
	};

	/**
	 * Handle mouse events on the orders tab table
	 */
	MouseAdapter ordersTabTableMouseAction = new MouseAdapter() {
		public void mouseDoubleClick(MouseEvent e) {
			if (e.button == 1) {
				openOrder(ordersTabTable.getItem(new Point(e.x, e.y)));
			}
		}

		public void mouseDown(MouseEvent e) {
			// Display popup menu
			if (e.button == 3) {
				TableItem tableItem = ordersTabTable.getItem(ordersTabTable.toControl(display.getCursorLocation()));
				clearPopupMenuItems(ordersTabTablePopupMenu);
				if (tableItem == null) {
					createMenuItem(ordersTabTablePopupMenu, SWT.NONE, "&New Order", "NewOrder", ordersTabTablePopupMenuItemClicked);
				} else {
					Order order = (Order)tableItem.getData();
					ordersTabTable.setSelection(tableItem);
					createMenuItem(ordersTabTablePopupMenu, SWT.NONE, "&New Order", "NewOrder", ordersTabTablePopupMenuItemClicked);
					createMenuItem(ordersTabTablePopupMenu, SWT.NONE, "&Open Order", "OpenOrder", ordersTabTablePopupMenuItemClicked);
					createMenuItem(ordersTabTablePopupMenu, SWT.NONE, "&Delete Order", "DeleteOrder", ordersTabTablePopupMenuItemClicked);
					
					if ((order.getOrderStatus().getDescription().equals("Delivered") || 
						order.getOrderStatus().getDescription().equals("Completed")) &&
						!order.getStockUpdated()) {
						new MenuItem(ordersTabTablePopupMenu, SWT.SEPARATOR);
						createMenuItem(ordersTabTablePopupMenu, SWT.NONE, "&Update Stock", "UpdateStock", ordersTabTablePopupMenuItemClicked);
					}
					
				}
			}
		}
	};

	/**
	 * Handle main menu selections
	 */
	SelectionAdapter mainMenuItemSelected = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			if (e.getSource().equals(exitMenuItem)) {
				shell.close();
			} else if (e.getSource().equals(exportStockMenuItem)) {
				exportStock();
			} else if (e.getSource().equals(importStockMenuItem)) {
				importStock();
			} else if (e.getSource().equals(helpMenuItem)) {
				AboutForm aboutForm = new AboutForm();
			}
		}
	};

	/**
	 * Handle tree item selection in stock tab tree
	 */
	SelectionAdapter stockTabTreeItemSelected = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			if (((Tree) e.getSource()).getSelection().length > 0) {
				searchStockTextBox.setText("");
				populateStockTabTable(null);
				setStatus();
			}
		}
	};

	/**
	 * Handle the selection of a new tab (stock or orders)
	 */
	SelectionAdapter mainTabFolderItemChanged = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			setStatus();
		}
	};
	
	/**
	 * Implement the search stock clear button listener
	 */
	SelectionAdapter searchStockClearButtonPressed = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			searchStockTextBox.setText("");
			populateStockTabTable(null);
		}
	};
	
	/**
	 * Implement the search orders clear button listener
	 */
	SelectionAdapter searchOrdersClearButtonPressed = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			searchOrdersTextBox.setText("");
			populateOrdersTabTable();
		}
	};	

	/**
	 * Clears all the items belonging to a popup menu
	 * 
	 * @param menu : The menu to be cleared
	 */
	private void clearPopupMenuItems(Menu menu) {
		for (int index = (menu.getItemCount() - 1); index >= 0; index--)
			menu.getItem(index).dispose();
	}

	/**
	 * Creates a popup menu item (when building a popup menu)
	 * 
	 * @param parentMenu : The popup menu this item belongs to
	 * @param style : Style attribute for the menu item
	 * @param text : The text to display in the menu item
	 * @param data : The data object associated with the menu item
	 * @param listener : The listener method to be called when the meni item is selected
	 * @return: The new menu item object
	 */
	private MenuItem createMenuItem(Menu parentMenu, int style, String text, Object data, SelectionListener listener) {
		MenuItem menuItem = new MenuItem(parentMenu, style);
		menuItem.setText(text);
		menuItem.setData(data);
		menuItem.addSelectionListener(listener);
		return (menuItem);
	}

	/**
	 * Populates the stock tab table based on the selected collection/group/category
	 * 
	 * @param: selectedObject: The selected collection/group/catergory
	 */
	private void populateStockTabTable(ObjectBase selectedObject) {

		// If no selected object was passed in we use the currently selected item from the tree
		if (selectedObject == null) {
			// Exit if nothing is selected (this should never happen!)
			if (stockTabTree.getSelection().length == 0) return;

			// Get the currently selected tree item
			TreeItem treeItem = stockTabTree.getSelection()[0];

			// Get the Collection/Group/Category object associated with the selected tree item
			if (treeItem != null) selectedObject = (ObjectBase) treeItem.getData();
		}

		// Populate the table based on the type of selected object
		if (selectedObject instanceof Collection) {
			// Collection was selected
			Collection collection = (Collection) selectedObject;
			stockTabTable.removeAll();
			Iterator iterator = Data.stock.iterator();
			while (iterator.hasNext()) {
				Item item = (Item) iterator.next();
				if (collection.getId() == item.getCollection().getId()) {
					setStockTableItem(new TableItem(stockTabTable, SWT.NONE), item);
				}
			}
		} else if (selectedObject instanceof Group) {
			// Group was selected
			Group group = (Group) selectedObject;
			stockTabTable.removeAll();
			Iterator iterator = Data.stock.iterator();
			while (iterator.hasNext()) {
				Item item = (Item) iterator.next();
				if (group.getId() == item.getGroup().getId()) {
					setStockTableItem(new TableItem(stockTabTable, SWT.NONE), item);
				}
			}
		} else if (selectedObject instanceof Category) {
			// Category was selected
			Category category = (Category) selectedObject;
			stockTabTable.removeAll();
			Iterator iterator = Data.stock.iterator();
			while (iterator.hasNext()) {
				Item item = (Item) iterator.next();
				if (category.getId() == item.getCategory().getId()) {
					setStockTableItem(new TableItem(stockTabTable, SWT.NONE), item);
				}
			}
		}
		sortTable(stockTabTable, stockTabTable.getSortColumn());
	}

	/**
	 * Populates the stock tab table with Items from a search
	 */
	private void populateStockTabTableBySearch(String searchTerm) {
		searchTerm = searchTerm.trim().toLowerCase();
		stockTabTable.removeAll();
		Iterator iterator = Data.stock.iterator();
		while (iterator.hasNext()) {
			Item item = (Item) iterator.next();
			if (item.getName().toLowerCase().indexOf(searchTerm) != -1) {
				setStockTableItem(new TableItem(stockTabTable, SWT.NONE), item);
			}
		}
		sortTable(stockTabTable, stockTabTable.getSortColumn());
	}

	/**
	 * Populates the orders tab table with Orders from a search
	 */
	private void populateOrdersTabTableBySearch(String searchTerm) {
		ordersTabTable.removeAll();
		Iterator orders = Data.orders.iterator();
		while (orders.hasNext()) {
			Order order = (Order) orders.next();
			if (order.getName().toLowerCase().indexOf(searchTerm) != -1) {
				setOrdersTableItem(new TableItem(ordersTabTable, SWT.NONE), order);
			} else {
				Iterator orderItems = order.orderItems.iterator();
				while (orderItems.hasNext()) {
					OrderItem orderItem = (OrderItem) orderItems.next();
					if (orderItem.getItem().getName().toLowerCase().indexOf(searchTerm) != -1) {
						setOrdersTableItem(new TableItem(ordersTabTable, SWT.NONE), order);
					}
				}
			}
		}

		sortTable(ordersTabTable, ordersTabTable.getSortColumn());
	}

	/**
	 * Implements the ability to edit a node in the tree
	 * 
	 * @param item : The tree item to be edited
	 */
	private void editTreeNode(final TreeItem item) {
		final TreeEditor editor = new TreeEditor(stockTabTree);
		boolean showBorder = true;
		final Composite composite = new Composite(stockTabTree, SWT.NONE);
		if (showBorder) composite.setBackground(display.getSystemColor(SWT.COLOR_BLACK));
		final Text text = new Text(composite, SWT.NONE);
		final int inset = showBorder ? 1 : 0;
		composite.addListener(SWT.Resize, new Listener() {
			public void handleEvent(Event e) {
				Rectangle rect = composite.getClientArea();
				text.setBounds(rect.x + inset, rect.y + inset, rect.width - inset * 2, rect.height - inset * 2);
			}
		});
		Listener textListener = new Listener() {
			public void handleEvent(final Event e) {
				switch (e.type) {
				case SWT.FocusOut:
					item.setText(text.getText());
					composite.dispose();
					updateCollectionGroupCategoryName(item);
					break;
				case SWT.Verify:
					String newText = text.getText();
					String leftText = newText.substring(0, e.start);
					String rightText = newText.substring(e.end, newText.length());
					GC gc = new GC(text);
					Point size = gc.textExtent(leftText + e.text + rightText);
					gc.dispose();
					size = text.computeSize(size.x, SWT.DEFAULT);
					editor.horizontalAlignment = SWT.LEFT;
					Rectangle itemRect = item.getBounds(),
					rect = stockTabTree.getClientArea();
					editor.minimumWidth = Math.max(size.x, itemRect.width) + inset * 2;
					int left = itemRect.x,
					right = rect.x + rect.width;
					editor.minimumWidth = Math.min(editor.minimumWidth, right - left);
					editor.minimumHeight = size.y + inset * 2;
					editor.layout();
					break;
				case SWT.Traverse:
					switch (e.detail) {
					case SWT.TRAVERSE_RETURN:
						item.setText(text.getText());
						updateCollectionGroupCategoryName(item);
						// Fall through
					case SWT.TRAVERSE_ESCAPE:
						composite.dispose();
						e.doit = false;
					}
					break;
				}
			}
		};
		text.addListener(SWT.FocusOut, textListener);
		text.addListener(SWT.Traverse, textListener);
		text.addListener(SWT.Verify, textListener);
		editor.setEditor(composite, item);
		text.setText(item.getText());
		text.selectAll();
		text.setFocus();
	}

	/**
	 * Updates the name of a Collection/Group/Category when a tree item has been edited
	 * 
	 * @param treeItem : The tree item that was edited
	 */
	private void updateCollectionGroupCategoryName(TreeItem treeItem) {
		ObjectBase object = (ObjectBase) treeItem.getData();
		if (object instanceof Collection) {
			((Collection) object).setName(treeItem.getText());
		} else if (object instanceof Group) {
			((Group) object).setName(treeItem.getText());
		} else if (object instanceof Category) {
			((Category) object).setName(treeItem.getText());
		}

		// Update the file record
		try {
			object.write();
		} catch (IOException e) {
			Errors.display(shell, "File Error", "Failed to update the name.", SWT.ICON_ERROR);
		}
	}

	/**
	 * Deletes a node from the tree Also removes its associated Collection/Groups/Categories
	 * 
	 * @param item : The tree item to be deleted
	 */
	private void deleteTreeNode(final TreeItem item) {
		boolean canDelete = true;

		// Check if collection/group/category is already assigned to an item
		Iterator iterator = Data.stock.iterator();
		if (item.getData() instanceof Collection) {
			Collection collection = (Collection) item.getData();
			while (iterator.hasNext()) {
				if (((Item) iterator.next()).getCollection().getId().equals(collection.getId())) {
					canDelete = false;
					break;
				}
			}
		} else if (item.getData() instanceof Group) {
			Group group = (Group) item.getData();
			while (iterator.hasNext()) {
				if (((Item) iterator.next()).getGroup().getId().equals(group.getId())) {
					canDelete = false;
					break;
				}
			}
		} else if (item.getData() instanceof Category) {
			Category category = (Category) item.getData();
			while (iterator.hasNext()) {
				if (((Item) iterator.next()).getCategory().getId().equals(category.getId())) {
					canDelete = false;
					break;
				}
			}
		}

		if (canDelete) {
			// The collection/group/category is not already assigned: just delete it
			deleteNodes(item);
		} else {
			// The collection/group/category is already assigned: display error message
			MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
			messageBox.setMessage("Unable to remove collection/group/category because it contains stock items.\n\nRemove all stock items and try again.");
			messageBox.open();
		}
	}

	/**
	 * Recursively deletes a tree node and all its children and their associated data objects
	 * 
	 * @param treeItem : The parent tree item to be deleted
	 */
	private void deleteNodes(TreeItem treeItem) {
		// Iterate through all the child tree items
		TreeItem[] childItems = treeItem.getItems();
		for (int index = (childItems.length - 1); index >= 0; index--) {
			// Recursivley call deleteNodes to remove all child nodes
			deleteNodes(childItems[index]);
		}

		// Delete the collection/group/category from the file
		ObjectBase object = (ObjectBase) treeItem.getData();
		try {
			object.remove();
		} catch (IOException e) {
			Errors.display(shell, "File Error", "Failed to delte the collection/group/category.", SWT.ICON_ERROR);
		}

		// Remove the group/category from its parent hash set
		if (treeItem.getParentItem() != null) {
			ObjectBase parentObject = (ObjectBase) treeItem.getParentItem().getData();
			if (parentObject instanceof Group) {
				((Group) parentObject).categories.remove(object.getId());
				Data.DeletedFilePointers.category.add(object.getRecordPointer());
			} else if (parentObject instanceof Collection) {
				((Collection) parentObject).groups.remove(object.getId());
				Data.DeletedFilePointers.group.add(object.getRecordPointer());
			}
		} else {
			Data.DeletedFilePointers.collection.add(object.getRecordPointer());
		}

		// Finally, delete the tree item
		treeItem.dispose();
	}

	/**
	 * Updates the main form status bar
	 */
	private void setStatus() {
		if (mainTabFolder.getSelection()[0].equals(stockTabItem)) {
			if (stockTabTree.getSelectionCount() > 0) {
				TreeItem item = stockTabTree.getSelection()[0];
				String location = "";
				while (item != null) {
					location = item.getText() + ((location == "") ? "" : "/") + location;
					item = item.getParentItem();
				}
				statusBarLabel1.setText(location);
				statusBarLabel1.setSize(statusBarLabel1.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				statusBarLabel2.setText(Integer.toString(stockTabTable.getItemCount()) + " items");
			} else {
				statusBarLabel1.setText("");
				statusBarLabel2.setText("");
			}
		} else if (mainTabFolder.getSelection()[0].equals(ordersTabItem)) {
			statusBarLabel1.setText("");
			statusBarLabel1.setSize(statusBarLabel1.computeSize(SWT.DEFAULT, SWT.DEFAULT));
			statusBarLabel2.setText(Integer.toString(ordersTabTable.getItemCount()) + " items");
		}
	}

	/**
	 * Opens an order (new or existing) in the order form
	 * 
	 * @param tableItem : The selected order in the table
	 */
	private Order openOrder(TableItem tableItem) {
		Order order = null;
		OrderForm orderForm = null;
		if (tableItem == null) {
			// Create a new order
			order = new Order(Files.orders, Data.DeletedFilePointers.order.poll());
			orderForm = new OrderForm(order);
			if (orderForm.OK()) {
				Data.orders.add(order.getId(), order);
				setOrdersTableItem(new TableItem(ordersTabTable, SWT.NONE), order);
				try {
					order.write();
				} catch (IOException e) {
					Errors.display(shell, "File Error", "Failed to save the order.", SWT.ICON_ERROR);
				}
			}
		} else {
			// Modify an existing order
			order = (Order) tableItem.getData();
			orderForm = new OrderForm(order);
			if (orderForm.OK()) {
				setOrdersTableItem(tableItem, order);
				try {
					order.write();
				} catch (IOException e) {
					Errors.display(shell, "File Error", "Failed to save the order.", SWT.ICON_ERROR);
				}
			}
		}
		return (order);
	}

	/**
	 * Opens a stock item (new or existing) in the stock item form
	 * 
	 * @param tableItem : The selected stock item in the table
	 */
	private Item openStockItem(TableItem tableItem) {
		Collection selectedCollection = null;
		Group selectedGroup = null;
		Category selectedCategory = null;
		Item item = null;
		StockItemForm stockItemForm = null;

		// Determine the currently selected Collection/Group/Category
		TreeItem treeItem = stockTabTree.getSelection()[0];
		ObjectBase selectedObject = (ObjectBase) treeItem.getData();
		if (selectedObject instanceof Collection) {
			selectedCollection = (Collection) selectedObject;
		} else if (selectedObject instanceof Group) {
			selectedGroup = (Group) selectedObject;
			selectedCollection = (Collection) treeItem.getParentItem().getData();
		} else if (selectedObject instanceof Category) {
			selectedCategory = (Category) selectedObject;
			selectedGroup = (Group) treeItem.getParentItem().getData();
			selectedCollection = (Collection) treeItem.getParentItem().getParentItem().getData();
		}

		if (tableItem == null) {
			// Create a new item
			item = new Item(Files.stock, Data.DeletedFilePointers.item.poll());
			stockItemForm = new StockItemForm(item, selectedCollection, selectedGroup, selectedCategory);
			if (stockItemForm.OK()) {
				Data.stock.add(item.getId(), item);
				setStockTableItem(new TableItem(stockTabTable, SWT.NONE), item);
				try {
					item.write();
				} catch (IOException e) {
					Errors.display(shell, "File Error", "Failed to save the stock item.", SWT.ICON_ERROR);
				}
			}
		} else {
			// Modify an existing item
			item = (Item) tableItem.getData();
			stockItemForm = new StockItemForm(item, selectedCollection, selectedGroup, selectedCategory);
			if (stockItemForm.OK()) {
				setStockTableItem(tableItem, item);
				try {
					item.write();
				} catch (IOException e) {
					Errors.display(shell, "File Error", "Failed to save the stock item.", SWT.ICON_ERROR);
				}
			}
		}

		// Update the stock table
		if (searchStockTextBox.getText().length() != 0) {
			populateStockTabTableBySearch(searchStockTextBox.getText());
		} else {
			populateStockTabTable(null);
		}

		return (item);
	}

	/**
	 * Removes a stock item
	 * 
	 * @param tableItem : The table item (stock item) to be removed
	 */
	private void removeStockItem(TableItem tableItem) {
		Item item = (Item) tableItem.getData();

		Iterator iterator = Data.orders.iterator();
		boolean itemOrdered = false;
		while (iterator.hasNext()) {
			Order order = (Order) iterator.next();
			if (order.orderItems.contains(item.getId())) {
				// Only allow items to be removed if the order is 'New' or 'Pending'
				if (order.getOrderStatus().getDescription().equals("Waiting for Vendor") || 
					order.getOrderStatus().getDescription().equals("Shipped")  || 
					order.getOrderStatus().getDescription().equals("Delivered")) {
					itemOrdered = true;
				}
				break;
			}
		}
		if (itemOrdered) {
			MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
			messageBox.setMessage("Unable to remove item because it is contained in one or more active orders.\n\nUpdate your orders and try again.");
			messageBox.open();
		} else {
			// Confirm that the user wants to delete this stock item
			MessageBox messageBox = new MessageBox(shell, SWT.ICON_QUESTION | SWT.YES | SWT.NO);
			messageBox.setMessage("Remove stock item.\n\nAre you sure?");
			if (messageBox.open() == SWT.YES) {
				// Remove the item from the hashset and the file
				Data.stock.remove(item.getId());
				try {
					item.remove();
				} catch (IOException e) {
					Errors.display(shell, "File Error", "Failed to remove the stock item.", SWT.ICON_ERROR);
				}

				// Update the deleted file pointers queue and remove the entry from the table
				Data.DeletedFilePointers.item.add(item.getRecordPointer());
				stockTabTable.remove(stockTabTable.getSelectionIndex());
			}
		}
	}

	/**
	 * Removes an order
	 * 
	 * @param tableItem : The table item (order) to be removed
	 */
	private void deleteOrder(TableItem tableItem) {
		Order order = (Order) tableItem.getData();

		if (order.getOrderStatus().getDescription().equals("New") || 
			order.getOrderStatus().getDescription().equals("Waiting for Vendor")) {
			// Confirm that the user wants to delete this stock item
			MessageBox messageBox = new MessageBox(shell, SWT.ICON_QUESTION | SWT.YES | SWT.NO);
			messageBox.setMessage("Delete order.\n\nAre you sure?");
			if (messageBox.open() == SWT.YES) {
				// Remove the item from the hashset and the file
				Data.orders.remove(order.getId());
				try {
					order.remove();
				} catch (IOException e) {
					Errors.display(shell, "File Error", "Failed to delete the order.", SWT.ICON_ERROR);
				}

				// Update the deleted file pointers queue and remove the entry from the table
				Data.DeletedFilePointers.order.add(order.getRecordPointer());
				ordersTabTable.remove(ordersTabTable.getSelectionIndex());
			}
		} else {
			MessageBox messageBox = new MessageBox(shell, SWT.ICON_INFORMATION | SWT.OK);
			messageBox.setMessage("Unable to delete this order.\n\nYou can only delete orders before they have shipped.");
			messageBox.open();
		}
	}
	
	/**
	 * Updates the stock from an order
	 * 
	 * @param tableItem : The table item (order) to be added to stock
	 */
	private void updateStock(TableItem tableItem) {
		Order order = (Order) tableItem.getData();
		
		// Confirm that the user wants to update stock from this order
		MessageBox messageBox = new MessageBox(shell, SWT.ICON_QUESTION | SWT.YES | SWT.NO);
		messageBox.setMessage("Update stock.\n\nAre you sure?");
		if (messageBox.open() == SWT.YES) {
			Iterator orderItems = order.orderItems.iterator();
			while (orderItems.hasNext()) {
				OrderItem orderItem = (OrderItem)orderItems.next();
				Item item = orderItem.getItem(); 
				item.setStockQuantity(item.getStockQuantity() + orderItem.getQuantity());
				try {
					item.write();
				} catch (IOException e) {
					Errors.display(shell, "File Error", "Failed to update stock.", SWT.ICON_ERROR);
				}
			}
			order.setStockUpdated(true);
			
			// Update the stock table
			if (searchStockTextBox.getText().length() != 0) {
				populateStockTabTableBySearch(searchStockTextBox.getText());
			} else {
				populateStockTabTable(null);
			}
		}
	}
	
	/**
	 * Adds the selected item to an order (via the OrderItem form)
	 * 
	 * @param tableItem : The item to be added to an order
	 */
	private void addToOrder(TableItem tableItem) {
		Item item = (Item) tableItem.getData();
		OrderItem orderItem = new OrderItem(Files.orderItems, null);

		// Make sure there is at least one 'open' order
		boolean ordersAvailable = false;
		Iterator orders = Data.orders.iterator();
		while (orders.hasNext()) {
			if (((Order) orders.next()).getOrderStatus().getDescription().equals("New") || ((Order) orders.next()).getOrderStatus().getDescription().equals("Waiting for Vendor")) {
				ordersAvailable = true;
				break;
			}
		}

		if (!ordersAvailable) {
			// No 'open' orders: offer to create one
			MessageBox messageBox = new MessageBox(shell, SWT.ICON_QUESTION | SWT.YES | SWT.NO);
			messageBox.setMessage("There are currently no open orders.\n\nDo you want to create a new order?");
			if (messageBox.open() == SWT.YES) {
				if (openOrder(null) == null) return;
			} else {
				return;
			}
		}
		// Display the OrderItem form
		OrderItemForm orderItemForm = new OrderItemForm(item, orderItem);
		if (orderItemForm.OK()) {
			
			// Check to see if this item already belongs to the selected order
			Iterator orderItems = ((Order)Data.orders.get(orderItem.getOrderId())).orderItems.iterator();
			while (orderItems.hasNext())
			{
				OrderItem duplicateOrderItem = (OrderItem)orderItems.next();
				if (duplicateOrderItem.getItem().getId().compareTo(item.getId()) == 0) {
					// The item already exists so we just update the existing record with the additional quantity
					duplicateOrderItem.setQuantity(duplicateOrderItem.getQuantity() + orderItem.getQuantity());
					orderItem = duplicateOrderItem;
				}
			}
			
			// Update the order (in the hashset and file)
			try {
				orderItem.write();
			} catch (IOException e) {
				Errors.display(shell, "File Error", "Failed to save the order item.", SWT.ICON_ERROR);
			}
			((Order) Data.orders.get(orderItem.getOrderId())).orderItems.add(orderItem.getId(), orderItem);
		}
	}

	/**
	 * Creates a new Collection and adds it to the hashset and stock tab tree
	 */
	private void newCollection() {
		TreeItem treeItem = new TreeItem(stockTabTree, SWT.None);
		Collection collection = new Collection(Files.collections, Data.DeletedFilePointers.collection.poll());
		collection.setName("New Collection");
		Data.collections.add(collection.getId(), collection);
		try {
			collection.write();
		} catch (IOException e) {
			Errors.display(shell, "File Error", "Failed to save the collection.", SWT.ICON_ERROR);
		}

		// Create the new tree item and leave it in edit mode for the user to enter the name
		treeItem.setText(collection.getName());
		treeItem.setImage(Data.images.get("FolderClosed"));
		treeItem.setData(collection);
		stockTabTree.showItem(treeItem);
		stockTabTree.select(treeItem);
		stockTabTree.redraw();
		editTreeNode(treeItem);
	}

	/**
	 * Creates a new Group and adds it to the hashset and stock tab tree
	 * 
	 * @param parentTreeItem : The (Collection) tree item that this Group will belong to
	 */
	private void newGroup(TreeItem parentTreeItem) {
		Collection collection = (Collection) parentTreeItem.getData();
		TreeItem treeItem = new TreeItem(parentTreeItem, SWT.None);
		Group group = new Group(Files.groups, Data.DeletedFilePointers.group.poll());
		group.setName("New Group");
		group.setCollectionID(collection.getId());
		collection.groups.add(group.getId(), group);
		try {
			group.write();
		} catch (IOException e) {
			Errors.display(shell, "File Error", "Failed to save the group.", SWT.ICON_ERROR);
		}

		// Create the new tree item and leave it in edit mode for the user to enter the name
		treeItem.setText(group.getName());
		treeItem.setImage(Data.images.get("FolderClosed"));
		treeItem.setData(group);
		stockTabTree.showItem(treeItem);
		stockTabTree.select(treeItem);
		editTreeNode(treeItem);
	}

	/**
	 * Creates a new Catergory and adds it to the hashset and stock tab tree
	 * 
	 * @param parentTreeItem : The (Group) tree item that this Category will belong to
	 */
	private void newCategory(TreeItem parentTreeItem) {
		Group group = (Group) parentTreeItem.getData();
		TreeItem treeItem = new TreeItem(parentTreeItem, SWT.None);
		Category category = new Category(Files.categories, Data.DeletedFilePointers.category.poll());
		category.setName("New Category");
		category.setGroupID(group.getId());
		group.categories.add(category.getId(), category);
		try {
			category.write();
		} catch (IOException e) {
			Errors.display(shell, "File Error", "Failed to save the category.", SWT.ICON_ERROR);
		}

		// Create the new tree item and leave it in edit mode for the user to enter the name
		treeItem.setText(category.getName());
		treeItem.setImage(Data.images.get("FolderClosed"));
		treeItem.setData(category);
		stockTabTree.showItem(treeItem);
		stockTabTree.select(treeItem);
		editTreeNode(treeItem);
	}

	/**
	 * Populates the stock tab tree with Collections/Groups/Categories
	 */
	private void populateStockTabTree() {
		Iterator collections = Data.collections.iterator();
		while (collections.hasNext()) {
			Collection collection = (Collection) collections.next();
			TreeItem collectionTreeItem = new TreeItem(stockTabTree, SWT.None);
			collectionTreeItem.setText(collection.getName());
			collectionTreeItem.setImage(Data.images.get("FolderClosed"));
			collectionTreeItem.setData(collection);

			Iterator groups = collection.groups.iterator();
			while (groups.hasNext()) {
				Group group = (Group) groups.next();
				TreeItem groupTreeItem = new TreeItem(collectionTreeItem, SWT.None);
				groupTreeItem.setText(group.getName());
				groupTreeItem.setImage(Data.images.get("FolderClosed"));
				groupTreeItem.setData(group);

				Iterator categories = group.categories.iterator();
				while (categories.hasNext()) {
					Category category = (Category) categories.next();
					TreeItem categoryTreeItem = new TreeItem(groupTreeItem, SWT.None);
					categoryTreeItem.setText(category.getName());
					categoryTreeItem.setImage(Data.images.get("FolderClosed"));
					categoryTreeItem.setData(category);
				}
			}
		}
	}

	/**
	 * Populates the orders tab table with Orders
	 */
	private void populateOrdersTabTable() {
		ordersTabTable.removeAll();
		Iterator iterator = Data.orders.iterator();
		while (iterator.hasNext()) {
			Order order = (Order) iterator.next();
			setOrdersTableItem(new TableItem(ordersTabTable, SWT.NONE), order);
		}
		sortTable(ordersTabTable, ordersTabTable.getSortColumn());
	}

	/**
	 * Adds an Order to the orders tab table
	 * 
	 * @param tableItem : The table item
	 * @param item : The Order
	 */
	private void setOrdersTableItem(TableItem tableItem, Order order) {
		// Set the correct icon based on the order status
		if (order.getOrderStatus().getDescription().equals("New")) {
			tableItem.setImage(Data.images.get("New"));
			tableItem.setText(0, "A");
		} else if (order.getOrderStatus().getDescription().equals("Waiting for Vendor")) {
			tableItem.setImage(Data.images.get("Waiting"));
			tableItem.setText(0, "B");
		} else if (order.getOrderStatus().getDescription().equals("Shipped")) {
			tableItem.setImage(Data.images.get("Shipped"));
			tableItem.setText(0, "C");
		} else if (order.getOrderStatus().getDescription().equals("Delivered")) {
			tableItem.setImage(Data.images.get("Delivered"));
			tableItem.setText(0, "D");
		} else if (order.getOrderStatus().getDescription().equals("Completed")) {
			tableItem.setImage(Data.images.get("Completed"));
			tableItem.setText(0, "E");
		} else if (order.getOrderStatus().getDescription().equals("Cancelled")) {
			tableItem.setImage(Data.images.get("Cancelled"));
			tableItem.setText(0, "F");
		} else if (order.getOrderStatus().getDescription().equals("Problem")) {
			tableItem.setImage(Data.images.get("Problem"));
			tableItem.setText(0, "G");
		}

		tableItem.setText(1, order.getName());
		tableItem.setText(2, DATE_FORMAT.format(order.getDate()));
		tableItem.setText(3, order.getOrderNumber());
		tableItem.setText(4, String.format("%10.2f", order.getTotalAmount()));
		tableItem.setData(order);
	}

	/**
	 * Adds an Item to the stock tab table
	 * 
	 * @param tableItem : The table item
	 * @param item : The stock Item
	 */
	private void setStockTableItem(TableItem tableItem, Item item) {
		// Set the correct icon based on the stock level
		if (item.getStockQuantity() == 0) {
			tableItem.setImage(Data.images.get("Cancelled"));
			tableItem.setText(0, "B");
		} else if (item.getStockQuantity() <= item.getReOrderLevel()) {
			tableItem.setImage(Data.images.get("AlertRed"));
			tableItem.setText(0, "A");
		}
		tableItem.setText(1, item.getName());
		tableItem.setText(2, Integer.toString(item.getStockQuantity()));
		tableItem.setData(item);
	}

	/**
	 * Sorts a table (bubble sort)
	 * 
	 * @param table: The table to be sorted
	 * @param column: The column to be sorted on
	 */
	private void sortTable(Table table, TableColumn column) {
		table.setLayoutDeferred(true);
		TableItem[] tableItems = table.getItems();
		for (int outer = 1; outer < tableItems.length; outer++) {
			String value1 = tableItems[outer].getText(table.indexOf(column));
			for (int inner = 0; inner < outer; inner++) {
				String value2 = tableItems[inner].getText(table.indexOf(column));
				if (compareValues(value1, value2, table.getSortDirection(), column.getData())) {
					if (table == stockTabTable) {
						setStockTableItem(new TableItem(table, SWT.NONE, inner), (Item) tableItems[outer].getData());
					} else {
						setOrdersTableItem(new TableItem(table, SWT.NONE, inner), (Order) tableItems[outer].getData());
					}
					tableItems[outer].dispose();
					tableItems = table.getItems();
					break;
				}
			}
		}
		table.setLayoutDeferred(false);
		table.setTopIndex(0);
	}

	/**
	 * Compares two values (used for sorting) See 'sortTable' method
	 * 
	 * @param value1: The first value to be compared
	 * @param value2: The second value to be compared
	 * @param direction: The sort direction (ascending/descending)
	 * @param type: The data type of the values being compared
	 * @return: true/false based on the comparison result
	 */
	private boolean compareValues(String value1, String value2, int direction, Object type) {
		if (type == String.class) {
			Collator collator = Collator.getInstance(Locale.getDefault());
			if (direction == SWT.UP) {
				return (collator.compare(value1, value2) > 0);
			} else {
				return (collator.compare(value1, value2) < 0);
			}
		} else if (type == Integer.class) {
			if (direction == SWT.UP) {
				return (Integer.parseInt(value1) > Integer.parseInt(value2));
			} else {
				return (Integer.parseInt(value1) < Integer.parseInt(value2));
			}
		} else if (type == Float.class) {
			if (direction == SWT.UP) {
				return (Float.parseFloat(value1) > Float.parseFloat(value2));
			} else {
				return (Float.parseFloat(value1) < Float.parseFloat(value2));
			}
		} else if (type == Date.class) {
			try {
				if (direction == SWT.UP) {
					return (DATE_FORMAT.parse(value1).after(DATE_FORMAT.parse(value2)));
				} else {
					return (DATE_FORMAT.parse(value1).before(DATE_FORMAT.parse(value2)));
				}
			} catch (ParseException e) {
				Errors.display(shell, "Sort Error", "The sort failed because the date was in the wrong format.", SWT.ICON_ERROR);
			}
		}
		return true;
	}

	/**
	 * Exports the stock hashset (items) to a file (comma-delimited)
	 */
	private void exportStock() {
		FileDialog saveFileDialog = new FileDialog(shell, SWT.SAVE);
		saveFileDialog.setText("Export Stock to...");
		String fileName = saveFileDialog.open();
		if (fileName != null) {
			File file = new File(fileName);
			if (file.exists()) {
				// The file already exists: confirm overwrite
				MessageBox messageBox = new MessageBox(saveFileDialog.getParent(), SWT.ICON_WARNING | SWT.YES | SWT.NO);
				messageBox.setMessage("The file already exists.\r\n\r\nOverwrite?");
				if (messageBox.open() != SWT.YES) return;
			}
			// Iterate through the stock hashset exporting each record to the file
			BufferedWriter fileWriter = null;
			try {
				fileWriter = new BufferedWriter(new FileWriter(file));
				Iterator iterator = Data.stock.iterator();
				while (iterator.hasNext()) {
					Item item = (Item) iterator.next();
					fileWriter.write(item.getExportData() + "\r\n");
				}
				fileWriter.close();
			} catch (IOException e) {
				Errors.display(shell, "File Error", "Failed to save the stock item.", SWT.ICON_ERROR);
			}

		}
	}

	/**
	 * Imports a file containing Stock Items and replaces the existing data
	 */
	private void importStock() {
		FileDialog openFileDialog = new FileDialog(shell, SWT.OPEN);
		openFileDialog.setText("Import Stock from...");
		String fileName = openFileDialog.open();
		if (fileName != null) {

			// Warn the user that all existing data will be overwritten
			MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING | SWT.YES | SWT.NO);
			messageBox.setMessage("Importing data will overwrite all existing data.\r\n\r\nAre you sure?");
			if (messageBox.open() != SWT.YES) return;

			// Delete all items from the hashset
			Iterator iterator = Data.stock.iterator();
			while (iterator.hasNext()) {
				Item item = (Item) iterator.next();
				try {
					item.remove();
				} catch (IOException e) {
					Errors.display(shell, "File Error", "Failed to remove the stock item.", SWT.ICON_ERROR);
				}
				Data.DeletedFilePointers.item.add(item.getRecordPointer());
			}
			Data.stock.Clear();

			// Open the import file and load the data
			File file = new File(fileName);
			BufferedReader fileReader = null;
			try {
				fileReader = new BufferedReader(new FileReader(file));
				String data = null;
				while ((data = fileReader.readLine()) != null) {
					Item item = new Item(Files.stock, Data.DeletedFilePointers.item.poll());
					if (data != null && !data.equals("")) {
						item.importData(data);
						Data.stock.add(item.getId(), item);
					}
				}
				fileReader.close();
			} catch (FileNotFoundException e) {
				Errors.display(shell, "File Error", "The file was not found.", SWT.ICON_ERROR);
			} catch (IOException e) {
				Errors.display(shell, "File Error", "Failed to load the stock item.", SWT.ICON_ERROR);
			}
		}

		// Select the first item in the tree (causes a re-load)
		if (stockTabTree.getItemCount() > 0) {
			stockTabTree.setSelection(stockTabTree.getItem(0));
			Event event = new Event();
			event.item = stockTabTree.getItem(0);
			stockTabTree.notifyListeners(SWT.Selection, event);
		}
	}

	/**
	 * Terminates the application (closes files etc.)
	 */
	private void TerminateApplication() {
		try {
			// Update configuration file with hashset sizes
			Data.saveHashsetSizes();

			// Close all the open files
			Files.stock.close();
			Files.orders.close();
			Files.orderItems.close();
			Files.collections.close();
			Files.groups.close();
			Files.categories.close();
			Files.vendors.close();
			Files.orderStatuses.close();
		} catch (Exception e) {
			Errors.display(shell, "File Error", "Unable to close one or more application data files.", SWT.ICON_ERROR);
		}
	}
}
