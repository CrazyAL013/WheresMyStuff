package forms;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

/**
 * Application Order Item Form UI
 * The UI code for the Order Item form (superclass)
 * This class contains code to create and display UI components
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 *
 */
public class OrderItemFormUI {
	
	// Control declarations (these controls are available to inheritors)
	protected Shell shell;
	protected Display display;
	protected Button okButton;
	protected Button cancelButton;
	protected Text orderQuantityTextBox;
	protected ComboViewer ordersComboViewer;
	protected Combo ordersComboBox;
	protected Label itemNameTextLabel;
	protected Label stockQuantityTextLabel;
	protected Label reOrderLevelTextLabel;

	public OrderItemFormUI() {
		display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
	}
	
	/**
	 * Create the window and its contents (controls) 
	 */	
	protected void createContents() {
		shell = new Shell(display, SWT.APPLICATION_MODAL | SWT.DIALOG_TRIM);
		shell.setMinimumSize(new Point(472, 360));
		this.shell.setSize(472, 360);
		this.shell.setText("Add to Order");
		GridLayout gl_shell = new GridLayout(2, false);
		gl_shell.marginRight = 10;
		gl_shell.marginLeft = 10;
		gl_shell.marginBottom = 10;
		gl_shell.marginTop = 10;
		shell.setLayout(gl_shell);
		
		Group grpStockItem = new Group(this.shell, SWT.NONE);
		GridLayout gl_grpStockItem = new GridLayout(6, false);
		gl_grpStockItem.marginTop = 10;
		gl_grpStockItem.marginRight = 10;
		gl_grpStockItem.marginLeft = 10;
		gl_grpStockItem.marginBottom = 10;
		grpStockItem.setLayout(gl_grpStockItem);
		grpStockItem.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		grpStockItem.setText("Stock Item");
		
		Label itemNameLabel = new Label(grpStockItem, SWT.NONE);
		GridData gd_itemNameLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_itemNameLabel.widthHint = 110;
		itemNameLabel.setLayoutData(gd_itemNameLabel);
		itemNameLabel.setText("Item Name:");
		
		itemNameTextLabel = new Label(grpStockItem, SWT.BORDER);
		itemNameTextLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 5, 1));
		
		Label stockQuantityLabel = new Label(grpStockItem, SWT.NONE);
		GridData gd_stockQuantityLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_stockQuantityLabel.widthHint = 110;
		stockQuantityLabel.setLayoutData(gd_stockQuantityLabel);
		stockQuantityLabel.setText("Stock Quantity:");
		
		stockQuantityTextLabel = new Label(grpStockItem, SWT.BORDER);
		GridData gd_stockQuantityTextLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_stockQuantityTextLabel.widthHint = 50;
		stockQuantityTextLabel.setLayoutData(gd_stockQuantityTextLabel);
		new Label(grpStockItem, SWT.NONE);
		
		Label reOrderLevelLabel = new Label(grpStockItem, SWT.NONE);
		reOrderLevelLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		reOrderLevelLabel.setText("Re-Order Level:");
		new Label(grpStockItem, SWT.NONE);
		
		reOrderLevelTextLabel = new Label(grpStockItem, SWT.BORDER);
		GridData gd_reOrderLevelTextLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_reOrderLevelTextLabel.widthHint = 50;
		reOrderLevelTextLabel.setLayoutData(gd_reOrderLevelTextLabel);
		
		Group orderGroup = new Group(this.shell, SWT.NONE);
		GridLayout gl_orderGroup = new GridLayout(3, false);
		gl_orderGroup.marginTop = 10;
		gl_orderGroup.marginRight = 10;
		gl_orderGroup.marginLeft = 10;
		gl_orderGroup.marginBottom = 10;
		orderGroup.setLayout(gl_orderGroup);
		orderGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		orderGroup.setText("Order");
		
		Label orderNameLabel = new Label(orderGroup, SWT.NONE);
		GridData gd_orderNameLabel = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gd_orderNameLabel.widthHint = 110;
		orderNameLabel.setLayoutData(gd_orderNameLabel);
		orderNameLabel.setText("Order Name:");
		
		ordersComboViewer = new ComboViewer(orderGroup, SWT.READ_ONLY);
		ordersComboBox = ordersComboViewer.getCombo();
		GridData gd_ordersComboBox = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gd_ordersComboBox.widthHint = 157;
		this.ordersComboBox.setLayoutData(gd_ordersComboBox);
		
		Group orderItemGroup = new Group(this.shell, SWT.NONE);
		GridLayout gl_orderItemGroup = new GridLayout(2, false);
		gl_orderItemGroup.marginTop = 10;
		gl_orderItemGroup.marginRight = 10;
		gl_orderItemGroup.marginLeft = 10;
		gl_orderItemGroup.marginBottom = 10;
		orderItemGroup.setLayout(gl_orderItemGroup);
		orderItemGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		orderItemGroup.setText("Order Item");
		
		Label orderQuantityLabel = new Label(orderItemGroup, SWT.NONE);
		GridData gd_orderQuantityLabel = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gd_orderQuantityLabel.widthHint = 110;
		orderQuantityLabel.setLayoutData(gd_orderQuantityLabel);
		orderQuantityLabel.setText("Order Quantity:");
		
		this.orderQuantityTextBox = new Text(orderItemGroup, SWT.BORDER | SWT.RIGHT);
		GridData gd_orderQuantityTextBox = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_orderQuantityTextBox.widthHint = 50;
		this.orderQuantityTextBox.setLayoutData(gd_orderQuantityTextBox);
		
		Composite composite_1 = new Composite(shell, SWT.NONE);
		GridLayout gl_composite_1 = new GridLayout(2, false);
		gl_composite_1.marginTop = 15;
		gl_composite_1.marginWidth = 0;
		gl_composite_1.verticalSpacing = 0;
		gl_composite_1.marginHeight = 0;
		composite_1.setLayout(gl_composite_1);
		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 7, 1));
		
		okButton = new Button(composite_1, SWT.CENTER);
		GridData gd_okButton = new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1);
		gd_okButton.minimumWidth = 68;
		gd_okButton.widthHint = 68;
		gd_okButton.heightHint = 28;
		gd_okButton.minimumHeight = 28;
		okButton.setLayoutData(gd_okButton);
		okButton.setText("OK");
		
		cancelButton = new Button(composite_1, SWT.NONE);
		GridData gd_cancelButton = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gd_cancelButton.heightHint = 28;
		gd_cancelButton.widthHint = 68;
		gd_cancelButton.minimumHeight = 28;
		gd_cancelButton.minimumWidth = 68;
		cancelButton.setLayoutData(gd_cancelButton);
		cancelButton.setText("Cancel");
	}
}
