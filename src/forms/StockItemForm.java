package forms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;

import global.*;
import model.*;

/**
 * Application Stock Item form. Displays a Stock Item (new or existing) that the user can modify.
 * This class contains code to implement the UI functionality.
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 * 
 */
public class StockItemForm extends StockItemFormUI {
	private Item item = null;
	private boolean result = false;
	private Control focusControl;

	public StockItemForm(Item item, Collection selectedCollection, Group selectedGroup, Category selectedCategory) {
		this.item = item;
		shell.setText("Stock Item");

		// Configure form fields
		this.nameTextBox.setTextLimit(150);
		this.stockQuantityTextBox.setTextLimit(6);
		this.reOrderLevelTextBox.setTextLimit(6);
		this.locationTextBox.setTextLimit(50);
		this.descriptionTextBox.setTextLimit(500);

		// Add UI listeners
		okButton.addSelectionListener(okButtonSelected);
		cancelButton.addSelectionListener(cancelButtonSelected);
		collectionComboBox.addSelectionListener(collectionComboBox_ItemSelected);
		groupComboBox.addSelectionListener(groupComboBox_ItemSelected);
		categoryComboBox.addSelectionListener(categoryComboBox_ItemSelected);
		stockQuantityTextBox.addVerifyListener(stockQuantityTextBoxVerify);
		reOrderLevelTextBox.addVerifyListener(reOrderLevelTextBoxVerify);

		// Populate the form fields/combos
		populateCollectionCombo(selectedCollection, selectedGroup, selectedCategory);
		populateForm();

		// Wait for user input
		while (!this.shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	public boolean OK() {
		return (this.result);
	}

	/**
	 * Implement the OK button listener
	 */
	SelectionAdapter okButtonSelected = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			if (stockQuantityTextBox.getText().equals("")) {
				MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
				messageBox.setMessage("No quantity was entered.\n\nPlease enter a quantity and try again.");
				messageBox.open();
				stockQuantityTextBox.selectAll();
				focusControl = stockQuantityTextBox;
				e.doit = false;
				
				Display.getCurrent().asyncExec(new Runnable() {
					public void run() {
						if (focusControl != null) focusControl.setFocus();
					}
				});	
			}
			
			if (reOrderLevelTextBox.getText().equals("")) {
				MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
				messageBox.setMessage("No re-order level was entered.\n\nPlease enter a re-order level and try again.");
				messageBox.open();
				reOrderLevelTextBox.selectAll();
				focusControl = reOrderLevelTextBox;
				e.doit = false;
				
				Display.getCurrent().asyncExec(new Runnable() {
					public void run() {
						if (focusControl != null) focusControl.setFocus();
					}
				});	
			}
			
			if (nameTextBox.getText().equals("")) {
				MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
				messageBox.setMessage("No name was entered.\n\nPlease enter a name and try again.");
				messageBox.open();
				nameTextBox.selectAll();
				focusControl = nameTextBox;
				e.doit = false;
				
				Display.getCurrent().asyncExec(new Runnable() {
					public void run() {
						if (focusControl != null) focusControl.setFocus();
					}
				});	
			}
			
			if (e.doit) {
				item.setName(nameTextBox.getText().trim());
				item.setStockQuantity(Integer.parseInt(stockQuantityTextBox.getText()));
				item.setReOrderLevel(Integer.parseInt(reOrderLevelTextBox.getText()));
				item.setCollection((Collection) ((IStructuredSelection) collectionComboViewer.getSelection()).getFirstElement());
				item.setGroup((Group) ((IStructuredSelection) groupComboViewer.getSelection()).getFirstElement());
				item.setCategory((Category) ((IStructuredSelection) categoryComboViewer.getSelection()).getFirstElement());
				item.setStorageLocation(locationTextBox.getText().trim());
				item.setDescription(descriptionTextBox.getText().trim());
				result = true;
				shell.close();
			}
		}
	};

	/**
	 * Implement the Cancel button listener
	 */
	SelectionAdapter cancelButtonSelected = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			shell.close();
		}
	};

	/**
	 * Implement the Collection combo item selected listener
	 */
	SelectionAdapter collectionComboBox_ItemSelected = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			populateGroupCombo(null);
		}
	};

	/**
	 * Implement the Group combo item selected listener
	 */
	SelectionAdapter groupComboBox_ItemSelected = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			populateCategoryCombo(null);
		}
	};

	/**
	 * Implement the Category combo item selected listener
	 */
	SelectionAdapter categoryComboBox_ItemSelected = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			// 
		}
	};

	/**
	 * Implement the verify listener to watch keypresses
	 */
	VerifyListener stockQuantityTextBoxVerify = new VerifyListener() {
		public void verifyText(VerifyEvent e) {
			e.doit = keyPressIsInteger(e.keyCode, e.character);
		}
	};
	
	/**
	 * Implement the verify listener to watch keypresses
	 */
	VerifyListener reOrderLevelTextBoxVerify = new VerifyListener() {
		public void verifyText(VerifyEvent e) {
			e.doit = keyPressIsInteger(e.keyCode, e.character);
		}
	};

	/**
	 * Populates the stock Item form fields
	 */
	private void populateForm() {
		nameTextBox.setText((this.item.getName() == null) ? "" : this.item.getName());
		stockQuantityTextBox.setText(String.format("%d", this.item.getStockQuantity()));
		reOrderLevelTextBox.setText(String.format("%d", this.item.getReOrderLevel()));
		if (this.item.getCollection() != null) {
			collectionComboViewer.setSelection(new StructuredSelection(this.item.getCollection()));
			collectionComboBox.notifyListeners(SWT.Selection, null);
		}
		if (this.item.getGroup() != null)  {
			groupComboViewer.setSelection(new StructuredSelection(this.item.getGroup()));
			groupComboBox.notifyListeners(SWT.Selection, null);
		}
		if (this.item.getCategory() != null)  {
			categoryComboViewer.setSelection(new StructuredSelection(this.item.getCategory()));
			categoryComboBox.notifyListeners(SWT.Selection, null);
		}
		
		locationTextBox.setText((this.item.getStorageLocation() == null) ? "" : this.item.getStorageLocation());
		descriptionTextBox.setText((this.item.getDescription() == null) ? "" : this.item.getDescription());
	}
	
	/**
	 * Validates integer key presses
	 * 
	 * @param keyCode: The key that was pressed
	 * @param character: The character that was entered
	 * @return: True if the key is allowed
	 */
	private boolean keyPressIsInteger(int keyCode, char character) {
		switch (keyCode) {
			case SWT.BS: // Backspace
			case SWT.DEL: // Delete
			case SWT.HOME: // Home
			case SWT.END: // End
			case SWT.ARROW_LEFT: // Left arrow
			case SWT.ARROW_RIGHT: // Right arrow
				return (true);
		}

		if (keyCode != 0 && !Character.isDigit(character)) {
			return (false); // disallow the action
		}
		return (true);
	}
	
	/**
	 * Populates the Collection combo
	 */
	private void populateCollectionCombo(Collection selectedCollection, Group selectedGroup, Category selectedCategory) {
		// Build an array of items to add to the combo list
		ArrayList<Collection> comboList = new ArrayList<Collection>();
		Iterator collections = Data.collections.iterator();
		while (collections.hasNext()) {
			comboList.add((Collection) collections.next());
		}
		
		// Sort the list and add it to the combo
		Collections.sort(comboList);
		collectionComboViewer.add(comboList.toArray());
			
		// Select the first entry
		if (collectionComboBox.getItemCount() > 0) {
			// Select the selected Collection
			collectionComboViewer.setSelection(new StructuredSelection(selectedCollection), true);
			// Now populate the groups/categories based on the selected collection
			populateGroupCombo(selectedGroup);
			populateCategoryCombo(selectedCategory);
		}
	}

	/**
	 * Populates the Group combo
	 */
	private void populateGroupCombo(Group selectedGroup) {
		groupComboViewer.getCombo().removeAll();
		categoryComboViewer.getCombo().removeAll();
		Collection collection = (Collection) ((IStructuredSelection) collectionComboViewer.getSelection()).getFirstElement();

		// Build an array of items to add to the combo list
		ArrayList<Group> comboList = new ArrayList<Group>();
		Iterator groups = collection.groups.iterator();
		while (groups.hasNext()) {
			comboList.add((Group) groups.next());
		}
		
		// Sort the list and add it to the combo
		Collections.sort(comboList);
		groupComboViewer.add(comboList.toArray());

		if (groupComboBox.getItemCount() > 0) {
			// if we have a selected category then select it otherwise select the first item in the combo
			if (selectedGroup != null)
				groupComboViewer.setSelection(new StructuredSelection(selectedGroup), true);
			else
				groupComboBox.select(0);
			groupComboBox.notifyListeners(SWT.Selection, null);
		}
	}

	/**
	 * Populates the Category combo
	 */
	private void populateCategoryCombo(Category selectedCategory) {
		categoryComboViewer.getCombo().removeAll();
		Group group = (Group) ((IStructuredSelection) groupComboViewer.getSelection()).getFirstElement();
		
		// Build an array of items to add to the combo list
		ArrayList<Category> comboList = new ArrayList<Category>();
		Iterator categories = group.categories.iterator();
		while (categories.hasNext()) {
			comboList.add((Category) categories.next());
		}
		
		// Sort the list and add it to the combo
		Collections.sort(comboList);
		categoryComboViewer.add(comboList.toArray());

		if (categoryComboBox.getItemCount() > 0) {
			// if we have a selected category then select it otherwise select the first item in the combo
			if (selectedCategory != null)
				categoryComboViewer.setSelection(new StructuredSelection(selectedCategory), true);
			else
				categoryComboBox.select(0);
			categoryComboBox.notifyListeners(SWT.Selection, null);
		}
	}
}