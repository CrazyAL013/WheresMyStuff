package forms;

import static global.Constants.DATE_FORMAT;

import java.io.IOException;
import java.text.Collator;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import model.Order;
import model.OrderItem;
import model.OrderStatus;
import model.Vendor;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import global.Data;
import global.Errors;


/**
 * Application Order form. Displays an Order (new or existing) that the user can modify.
 * This class contains code to implement the UI functionality
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 * 
 */
public class OrderForm extends OrderFormUI {
	private Order order = null;
	private boolean result = false;
	private Control focusControl;

	public OrderForm(Order order) {
		this.order = order;
		shell.setText("Order Form");
		
		// Configure form fields
		orderItemsTable.setSortColumn(itemNameTableColumn);
		orderItemsTable.setSortDirection(SWT.DOWN);

		this.nameTextBox.setTextLimit(50);
		this.dateTextBox.setTextLimit(11);
		this.orderNumberTextBox.setTextLimit(25);
		this.totalTextBox.setTextLimit(8);
		this.notesTextBox.setTextLimit(500);

		// Populate the form fields/combos/table
		populateVendorCombo();
		populateOrderStatusCombo();
		populateForm();
		populateOrderItemsTable();

		// Add UI listeners
		okButton.addSelectionListener(okButtonSelected);
		cancelButton.addSelectionListener(cancelButtonSelected);
		totalTextBox.addVerifyListener(totalTextBoxVerify);
		orderItemsTable.addMouseListener(orderItemsTableMouseAction);
		
		itemNameTableColumn.addSelectionListener(orderItemsTableColumnSelectionListener);
		stockQuantityTableColumn.addSelectionListener(orderItemsTableColumnSelectionListener);
		quantityTableColumn.addSelectionListener(orderItemsTableColumnSelectionListener);

		// Wait for user input
		while (!this.shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	public boolean OK() {
		return (this.result);
	}

	/**
	 * Implement the OK button listener
	 */
	SelectionAdapter okButtonSelected = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			// Validate the field(s)
			try {
				order.setDate(DATE_FORMAT.parse(dateTextBox.getText()));
			} catch (ParseException e1) {
				MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
				messageBox.setMessage("The date is invalid.\n\nPlease enter a valid date in the format dd-mmm-yyyy (13-Nov-2012)");
				messageBox.open();
				dateTextBox.selectAll();
				focusControl = dateTextBox;
				e.doit = false;
				
				Display.getCurrent().asyncExec(new Runnable() {
					public void run() {
						if (focusControl != null) focusControl.setFocus();
					}
				});
			}
			
			if (totalTextBox.getText().equals("")) {
				MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
				messageBox.setMessage("No total amount was entered.\n\nPlease enter a total amount and try again.");
				messageBox.open();
				totalTextBox.selectAll();
				focusControl = totalTextBox;
				e.doit = false;
				
				Display.getCurrent().asyncExec(new Runnable() {
					public void run() {
						if (focusControl != null) focusControl.setFocus();
					}
				});	
			}
			
			if (nameTextBox.getText().equals("")) {
				MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
				messageBox.setMessage("No name was entered.\n\nPlease enter a name and try again.");
				messageBox.open();
				nameTextBox.selectAll();
				focusControl = nameTextBox;
				e.doit = false;
				
				Display.getCurrent().asyncExec(new Runnable() {
					public void run() {
						if (focusControl != null) focusControl.setFocus();
					}
				});	
			}
			
			if (e.doit) {
				// Save the fields to the object and close the form
				order.setName(nameTextBox.getText().trim());
				order.setTotalAmount(Float.parseFloat(totalTextBox.getText().trim()));
				order.setOrderNumber(orderNumberTextBox.getText());
				order.setVendor((Vendor) ((IStructuredSelection) vendorComboViewer.getSelection()).getFirstElement());
				order.setOrderStatus((OrderStatus) ((IStructuredSelection) orderStatusComboViewer.getSelection()).getFirstElement());
				order.setNotes(notesTextBox.getText());
				result = true;
				shell.close();
			}
		}
	};

	/**
	 * Implement the Cancel button listener
	 */
	SelectionAdapter cancelButtonSelected = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			shell.close();
		}
	};

	/**
	 * Implement the verify listener to watch keypresses
	 */
	VerifyListener totalTextBoxVerify = new VerifyListener() {
		public void verifyText(VerifyEvent e) {
			e.doit = keyPressIsDecimal(e.keyCode, e.character);
		}
	};

	/**
	 * Handle mouse events on the stock tab tree
	 */
	MouseAdapter orderItemsTableMouseAction = new MouseAdapter() {
		public void mouseDoubleClick(MouseEvent e) {
			if (e.button == 1) {
				openOrderItem(orderItemsTable.getItem(new Point(e.x, e.y)));
			}
		}
		
		public void mouseDown(MouseEvent e) {
			// Display popup menu
			if (e.button == 3) {
				TableItem tableItem = orderItemsTable.getItem(orderItemsTable.toControl(display.getCursorLocation()));
				clearPopupMenuItems(orderItemsTablePopupMenu);
				createMenuItem(orderItemsTablePopupMenu, SWT.NONE, "&Open Order Item", "OpenOrderItem", orderItemsTablePopupMenuItemClicked);
				new MenuItem(orderItemsTablePopupMenu, SWT.SEPARATOR);
				createMenuItem(orderItemsTablePopupMenu, SWT.NONE, "&Remove Item", "RemoveItem", orderItemsTablePopupMenuItemClicked);
			}
		}
	};

	/**
	 * Handle orders tab table popup menu selections
	 */
	SelectionAdapter orderItemsTablePopupMenuItemClicked = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			TableItem tableItem = null;
			if (orderItemsTable.getSelectionCount() > 0) {
				tableItem = orderItemsTable.getSelection()[0];
			}

			if (((MenuItem) e.getSource()).getData().equals("RemoveItem")) {
				removeOrderItem(tableItem);
			} else if (((MenuItem) e.getSource()).getData().equals("OpenOrderItem")) {
				openOrderItem(tableItem);
			}
		}
	};
	
	/**
	 * Implement order items table column selection listener
	 */
	SelectionAdapter orderItemsTableColumnSelectionListener = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			// The table column header was clicked: Sort the table
			TableColumn selectedColumn = (TableColumn) e.widget;
			if (orderItemsTable.getSortColumn() == selectedColumn) {
				orderItemsTable.setSortDirection((orderItemsTable.getSortDirection() == SWT.UP) ? SWT.DOWN : SWT.UP);
			} else {
				orderItemsTable.setSortColumn(selectedColumn);
				orderItemsTable.setSortDirection(SWT.UP);
			}
			sortTable(orderItemsTable, selectedColumn);
		}
	};
	
	/**
	 * Opens a order item in the order item form
	 * 
	 * @param tableItem : The selected order item in the table
	 */
	private OrderItem openOrderItem(TableItem tableItem) {
			OrderItemForm orderItemForm = null;
			// Modify an existing order
			OrderItem orderItem = (OrderItem) tableItem.getData();
			orderItemForm = new OrderItemForm(orderItem.getItem(), orderItem);
			if (orderItemForm.OK()) {
				setOrderItemsTableItem(tableItem, orderItem);
				try {
					orderItem.write();
				} catch (IOException e) {
					Errors.display(shell, "File Error", "Failed to save the order item.", SWT.ICON_ERROR);
				}
			}

		// Update the order table
		populateOrderItemsTable();
		return (orderItem);
	}

	/**
	 * Remove an order item from the order
	 * 
	 * @param tableItem: The order item to be removed
	 */
	private void removeOrderItem(TableItem tableItem) {
		OrderItem orderItem = (OrderItem) tableItem.getData();

		// Only allow changes to orders that are 'New' or 'Pending'
		if (order.getOrderStatus().getDescription().equals("New") || order.getOrderStatus().getDescription().equals("Pending")) {
			// Confirm that the user wants to delete this stock item
			MessageBox messageBox = new MessageBox(shell, SWT.ICON_QUESTION | SWT.YES | SWT.NO);
			messageBox.setMessage("Remove item from order.\n\nAre you sure?");
			if (messageBox.open() == SWT.YES) {
				// Remove the item from the hashset and the file
				order.orderItems.remove(orderItem.getId());
				try {
					orderItem.remove();
				} catch (IOException e) {
					Errors.display(shell, "File Error", "Failed to remove the order item.", SWT.ICON_ERROR);
				}

				// Update the deleted file pointers queue and remove the entry from the table
				Data.DeletedFilePointers.orderItems.add(orderItem.getRecordPointer());
				orderItemsTable.remove(orderItemsTable.getSelectionIndex());
			}
		} else {
			MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
			messageBox.setMessage("Unable to remove item because the order is complete.");
			messageBox.open();
		}
	}

	/**
	 * Creates a popup menu item (when building a popup menu)
	 * 
	 * @param parentMenu : The popup menu this item belongs to
	 * @param style : Style attribute for the menu item
	 * @param text : The text to display in the menu item
	 * @param data : The data object associated with the menu item
	 * @param listener : The listener method to be called when the meni item is selected
	 * @return: The new menu item object
	 */
	private MenuItem createMenuItem(Menu parentMenu, int style, String text, Object data, SelectionListener listener) {
		MenuItem menuItem = new MenuItem(parentMenu, style);
		menuItem.setText(text);
		menuItem.setData(data);
		menuItem.addSelectionListener(listener);
		return (menuItem);
	}

	/**
	 * Clears all the items belonging to a popup menu
	 * 
	 * @param menu : The menu to be cleared
	 */
	private void clearPopupMenuItems(Menu menu) {
		for (int index = (menu.getItemCount() - 1); index >= 0; index--)
			menu.getItem(index).dispose();
	}

	/**
	 * Validates decimal key presses
	 * 
	 * @param keyCode: The key that was pressed
	 * @param character: The character that was entered
	 * @return: True if the key is allowed
	 */
	private boolean keyPressIsDecimal(int keyCode, char character) {
		switch (keyCode) {
		case SWT.BS: // Backspace
		case SWT.DEL: // Delete
		case SWT.HOME: // Home
		case SWT.END: // End
		case SWT.ARROW_LEFT: // Left arrow
		case SWT.ARROW_RIGHT: // Right arrow
			return (true);
		}

		if (keyCode != 0 && !Character.isDigit(character) && character != '.') {
			return (false); // disallow the action
		}
		return (true);
	}

	/**
	 * Populates the Order form fields
	 */
	private void populateForm() {
		nameTextBox.setText((this.order.getName() == null) ? "" : this.order.getName());
		dateTextBox.setText((this.order.getDate() == null) ? DATE_FORMAT.format(Calendar.getInstance().getTime()) : DATE_FORMAT.format((order.getDate())));
		orderNumberTextBox.setText((this.order.getOrderNumber() == null) ? "" : this.order.getOrderNumber());
		totalTextBox.setText(String.format("%.2f", this.order.getTotalAmount()));
		notesTextBox.setText((this.order.getNotes() == null) ? "" : this.order.getNotes());
		if (this.order.getVendor() != null) vendorComboViewer.setSelection(new StructuredSelection(this.order.getVendor()));
		if (this.order.getOrderStatus() != null) orderStatusComboViewer.setSelection(new StructuredSelection(this.order.getOrderStatus()));
	}

	/**
	 * Populates the vendor combo
	 */
	private void populateVendorCombo() {
		// Build an array of items to add to the combo list
		ArrayList<Vendor> comboList = new ArrayList<Vendor>();
		Iterator vendors = Data.vendors.iterator();
		while (vendors.hasNext()) {
			comboList.add((Vendor) vendors.next());
		}

		// Sort the list, add it to the combo and select the first entry
		Collections.sort(comboList);
		vendorComboViewer.add(comboList.toArray());
		if (vendorComboBox.getItemCount() > 0) vendorComboBox.select(0);
	}

	/**
	 * Populates the status combo
	 */
	private void populateOrderStatusCombo() {
		// Build an array of items to add to the combo list
		ArrayList<OrderStatus> comboList = new ArrayList<OrderStatus>();
		Iterator orderStatuses = Data.orderStatuses.iterator();
		while (orderStatuses.hasNext()) {
			comboList.add((OrderStatus) orderStatuses.next());
		}

		// Sort the list, add it to the combo and select the 'New' entry
		Collections.sort(comboList);
		orderStatusComboViewer.add(comboList.toArray());
		if (orderStatusComboBox.indexOf("New") > 0) orderStatusComboBox.select(orderStatusComboBox.indexOf("New"));
	}

	/**
	 * Populates the Order Items table
	 */
	private void populateOrderItemsTable() {
		orderItemsTable.removeAll();
		Iterator iterator = this.order.orderItems.iterator();
		while (iterator.hasNext()) {
			OrderItem orderItem = (OrderItem) iterator.next();
			setOrderItemsTableItem(new TableItem(orderItemsTable, SWT.NONE), orderItem);
		}
	}

	/**
	 * Adds an OrderItem to the order items table
	 * @param tableItem: The table item
	 * @param item: The OrderItem
	 */
	private void setOrderItemsTableItem(TableItem tableItem, OrderItem orderItem) {
		if (orderItem.getItem() != null) {
			tableItem.setText(0, orderItem.getItem().getName());
			tableItem.setText(1,Integer.toString(orderItem.getItem().getStockQuantity()));
		} else {
			// Item was probably deleted: Display placeholder text
			tableItem.setText(0, "<<Item no longer exists>>");
			tableItem.setText(1, "0");
		}

		tableItem.setText(2, Integer.toString(orderItem.getQuantity()));
		tableItem.setData(orderItem);
	}
	
	/**
	 * Sorts a table (bubble sort)
	 * 
	 * @param table: The table to be sorted
	 * @param column: The column to be sorted on
	 */
	private void sortTable(Table table, TableColumn column) {
		table.setLayoutDeferred(true);
		TableItem[] tableItems = table.getItems();
		for (int outer = 1; outer < tableItems.length; outer++) {
			String value1 = tableItems[outer].getText(table.indexOf(column));
			for (int inner = 0; inner < outer; inner++) {
				String value2 = tableItems[inner].getText(table.indexOf(column));
				if (compareValues(value1, value2, table.getSortDirection(), column.getData())) {
					if (table == orderItemsTable) {
						setOrderItemsTableItem(new TableItem(table, SWT.NONE, inner), (OrderItem) tableItems[outer].getData());
					} 
					tableItems[outer].dispose();
					tableItems = table.getItems();
					break;
				}
			}
		}
		table.setLayoutDeferred(false);
		table.setTopIndex(0);
	}

	/**
	 * Compares two values (used for sorting) See 'sortTable' method
	 * 
	 * @param value1: The first value to be compared
	 * @param value2: The second value to be compared
	 * @param direction: The sort direction (ascending/descending)
	 * @param type: The data type of the values being compared
	 * @return: true/false based on the comparison result
	 */
	private boolean compareValues(String value1, String value2, int direction, Object type) {
		if (type == String.class) {
			Collator collator = Collator.getInstance(Locale.getDefault());
			if (direction == SWT.UP) {
				return (collator.compare(value1, value2) > 0);
			} else {
				return (collator.compare(value1, value2) < 0);
			}
		} else if (type == Integer.class) {
			if (direction == SWT.UP) {
				return (Integer.parseInt(value1) > Integer.parseInt(value2));
			} else {
				return (Integer.parseInt(value1) < Integer.parseInt(value2));
			}
		} else if (type == Float.class) {
			if (direction == SWT.UP) {
				return (Float.parseFloat(value1) > Float.parseFloat(value2));
			} else {
				return (Float.parseFloat(value1) < Float.parseFloat(value2));
			}
		} else if (type == Date.class) {
			try {
				if (direction == SWT.UP) {
					return (DATE_FORMAT.parse(value1).after(DATE_FORMAT.parse(value2)));
				} else {
					return (DATE_FORMAT.parse(value1).before(DATE_FORMAT.parse(value2)));
				}
			} catch (ParseException e) {
				Errors.display(shell, "Sort Error", "The sort failed because the date was in the wrong format.", SWT.ICON_ERROR);
			}
		}
		return true;
	}
}