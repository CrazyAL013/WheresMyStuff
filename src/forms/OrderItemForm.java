package forms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import model.Item;
import model.Order;
import model.OrderItem;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;

import global.Data;

/**
 * Application Order Item form. Displays a stock Item that the user can add to an Order. 
 * This class contains code to implement the UI functionality
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 * 
 */
public class OrderItemForm extends OrderItemFormUI {
	private Item item = null;
	private OrderItem orderItem = null;
	private boolean result = false;
	private Control focusControl;

	public OrderItemForm(Item item, OrderItem orderItem) {
		this.item = item;
		this.orderItem = orderItem;

		// Configure form fields
		this.orderQuantityTextBox.setTextLimit(6);
		this.orderQuantityTextBox.setText("1");
		this.orderQuantityTextBox.selectAll();
		
		// Populate the form fields/combo
		populateOrdersCombo();
		populateForm();

		// Add UI listeners
		okButton.addSelectionListener(okButtonSelected);
		cancelButton.addSelectionListener(cancelButtonSelected);
		orderQuantityTextBox.addVerifyListener(orderQuantityTextBoxVerify);

		// Wait for user input
		while (!this.shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	public boolean OK() {
		return (this.result);
	}

	/**
	 * Implement the OK button listener
	 */
	SelectionAdapter okButtonSelected = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {

			if (((IStructuredSelection) ordersComboViewer.getSelection()).size() == 0) {
				// No order was selected
				MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
				messageBox.setMessage("No order was selected.\n\nPlease select an order.");
				messageBox.open();
				focusControl = ordersComboBox;
				e.doit = false;

				Display.getCurrent().asyncExec(new Runnable() {
					public void run() {
						if (focusControl != null) focusControl.setFocus();
					}
				});
			}

			if (orderQuantityTextBox.getText().equals("")) {
				MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
				messageBox.setMessage("No quantity was entered.\n\nPlease enter a quantity and try again.");
				messageBox.open();
				orderQuantityTextBox.selectAll();
				focusControl = orderQuantityTextBox;
				e.doit = false;
				
				Display.getCurrent().asyncExec(new Runnable() {
					public void run() {
						if (focusControl != null) focusControl.setFocus();
					}
				});	
			}
			
			if (e.doit) {
				Order order = ((Order) ((IStructuredSelection) ordersComboViewer.getSelection()).getFirstElement());
				orderItem.setItem(item);
				orderItem.setorderId(order.getId());
				orderItem.setQuantity(Integer.parseInt(orderQuantityTextBox.getText()));
				result = true;
				shell.close();
			}
		}
	};

	/**
	 * Implement the Cancel button listener
	 */
	SelectionAdapter cancelButtonSelected = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			shell.close();
		}
	};

	/**
	 * Implement the verify listener to watch keypresses
	 */
	VerifyListener orderQuantityTextBoxVerify = new VerifyListener() {
		public void verifyText(VerifyEvent e) {
			e.doit = keyPressIsInteger(e.keyCode, e.character);
		}
	};

	/**
	 * Populates the Order form fields
	 */
	private void populateForm() {
		itemNameTextLabel.setText((this.item.getName() == null) ? "" : this.item.getName());
		stockQuantityTextLabel.setText(String.format("%d", this.item.getStockQuantity()));
		reOrderLevelTextLabel.setText(String.format("%d", this.item.getReOrderLevel()));
		
		if (orderItem.getOrderId() != null) {
			orderQuantityTextBox.setText(String.format("%d", orderItem.getQuantity()));
		}
	}

	/**
	 * Validates integer key presses
	 * 
	 * @param keyCode: The key that was pressed
	 * @param character: The character that was entered
	 * @return: True if the key is allowed
	 */
	private boolean keyPressIsInteger(int keyCode, char character) {
		switch (keyCode) {
			case SWT.BS: // Backspace
			case SWT.DEL: // Delete
			case SWT.HOME: // Home
			case SWT.END: // End
			case SWT.ARROW_LEFT: // Left arrow
			case SWT.ARROW_RIGHT: // Right arrow
				return (true);
		}

		if (keyCode != 0 && !Character.isDigit(character)) {
			return (false); // disallow the action
		}
		return (true);
	}

	/**
	 * Populates the orders combo
	 */
	private void populateOrdersCombo() {
		// Build an array of items to add to the combo list
		ArrayList<Order> comboList = new ArrayList<Order>();
		Iterator orders = Data.orders.iterator();
		while (orders.hasNext()) {
			Order order = (Order) orders.next();
			if (orderItem.getOrderId() == null) {
				// Only add 'New' orders to the list
				if (order.getOrderStatus().getDescription().equals("New") || order.getOrderStatus().getDescription().equals("Waiting for Vendor")) comboList.add(order);
			} else {
				if (order.getId().compareTo(orderItem.getOrderId()) == 0) {
					comboList.add(order);
				}
			}
		}

		// Sort the list, add it to the combo and select the 'New' entry
		Collections.sort(comboList);
		ordersComboViewer.add(comboList.toArray());
		
		if (ordersComboBox.getItemCount() > 0) ordersComboBox.select(0);
		
	}
}