package model;

import global.Data;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.UUID;

/**
 * OrderItem Class Represents a single order item
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 * 
 */
public class OrderItem extends ObjectBase {
	private int quantity;
	private UUID orderId;
	private Item item;

	public OrderItem(RandomAccessFile file, Long recordPointer) {
		super(file, recordPointer);
	}

	public void read() throws IOException {
			file.seek(this.recordPointer);
			this.id = UUID.fromString(readString(36));
			this.orderId = UUID.fromString(readString(36));
			this.item = (Item) Data.stock.get(UUID.fromString(readString(36)));
			this.quantity = file.readInt();
	}

	public void write() throws IOException {
			// If record pointer is null: add record to the end of the file
			if (this.recordPointer == null) this.recordPointer = file.length();
			file.seek(this.recordPointer);

			writeString(this.id.toString(), 36);
			writeString(this.orderId.toString(), 36);
			writeString(this.item.getId().toString(), 36);
			file.writeInt(this.quantity);
	}

	public UUID getOrderId() {
		return (this.orderId);
	}

	public void setorderId(UUID orderId) {
		this.orderId = orderId;
	}

	public Item getItem() {
		return (this.item);
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public int getQuantity() {
		return (this.quantity);
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
