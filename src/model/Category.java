package model;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.UUID;

/**
 * Category Class Represents a single category
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 * 
 */
public class Category extends ObjectBase implements Comparable<Category> {
	private String name;
	private UUID groupID;

	public Category(RandomAccessFile file, Long recordPointer)  {
		super(file, recordPointer);
	}

	public void read() throws IOException {
			file.seek(this.recordPointer);
			this.id = UUID.fromString(readString(36));
			this.name = readString(100);
			this.groupID = UUID.fromString(readString(36));
	}

	public void write() throws IOException {
			// If record pointer is null: add record to the end of the file
			if (this.recordPointer == null) this.recordPointer = file.length();
			file.seek(this.recordPointer);

			writeString(this.id.toString(), 36);
			writeString(this.name, 100);
			writeString(this.groupID.toString(), 36);
	}

	public String getName() {
		return (this.name);
	}

	public void setName(String name) {
		this.name = name;
	}

	public UUID getGroupID() {
		return (this.groupID);
	}

	public void setGroupID(UUID groupID) {
		this.groupID = groupID;
	}

	@Override
	public String toString() {
		return this.name;
	}
	
	@Override
	public int compareTo(Category category) {
		return (this.name.compareTo(category.name));
	}
}
