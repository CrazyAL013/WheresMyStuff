package model;

import static global.Constants.DATE_FORMAT;
import global.Data;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.ParseException;
import java.util.Date;
import java.util.UUID;

/**
 * Order Class Represents a single order
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 * 
 */
public class Order extends ObjectBase implements Comparable<Order> {
	private String name;
	private Date date;
	private String orderNumber;
	private float totalAmount;
	public HashSet orderItems = new HashSet(50);
	private Vendor vendor;
	private OrderStatus orderStatus;
	private String notes;
	private boolean stockUpdated = false;

	public Order(RandomAccessFile file, Long recordPointer) {
		super(file, recordPointer);
	}

	public void read() throws IOException, ParseException {
			file.seek(this.recordPointer);
			this.id = UUID.fromString(readString(36));
			this.name = readString(50);
			this.date = DATE_FORMAT.parse(readString(11));
			this.orderNumber = readString(25);
			this.totalAmount = file.readFloat();
			this.vendor = (Vendor) Data.vendors.get(UUID.fromString(readString(36)));
			this.orderStatus = (OrderStatus) Data.orderStatuses.get(UUID.fromString(readString(36)));
			this.notes = readString(500);
			this.stockUpdated = file.readBoolean();
			
			// Resize the order items hashset based on configuration data
			orderItems = null;
			orderItems = new HashSet(Integer.parseInt(Data.config.getProperty(id.toString(), "50")));
	}

	public void write() throws IOException {
			// If record pointer is null: add record to the end of the file
			if (this.recordPointer == null) this.recordPointer = file.length();
			file.seek(this.recordPointer);

			writeString(this.id.toString(), 36);
			writeString(this.name, 50);
			writeString(DATE_FORMAT.format(this.date), 11);
			writeString(this.orderNumber, 25);
			file.writeFloat(this.totalAmount);
			writeString(this.vendor.getId().toString(), 36);
			writeString(this.orderStatus.getId().toString(), 36);
			writeString(this.notes, 500);
			file.writeBoolean(this.stockUpdated);
	}

	public String getName() {
		return (this.name);
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return (this.date);
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getOrderNumber() {
		return (this.orderNumber);
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public float getTotalAmount() {
		return (this.totalAmount);
	}

	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Vendor getVendor() {
		return (this.vendor);
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}
	
	public OrderStatus getOrderStatus() {
		return (this.orderStatus);
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}
	
	public String getNotes() {
		return (this.notes);
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	public Boolean getStockUpdated() {
		return (this.stockUpdated);
	}

	public void setStockUpdated(boolean stockUpdated) {
		this.stockUpdated = stockUpdated;
	}
	
	public String toString() {
		return this.name;
	}

	@Override
	public int compareTo(Order order) {
		return (this.name.compareTo(order.name));
	}
}
