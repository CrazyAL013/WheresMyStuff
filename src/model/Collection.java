package model;

import global.Data;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.UUID;

/**
 * Collection Class Represents a single collection
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 * 
 */
public class Collection extends ObjectBase implements Comparable<Collection> {
	private String name;
	public HashSet groups = new HashSet(50);

	public Collection(RandomAccessFile file, Long recordPointer) {
		super(file, recordPointer);
	}

	public void read() throws IOException {
			file.seek(this.recordPointer);
			this.id = UUID.fromString(readString(36));
			this.name = readString(100);

			// Resize the groups hashset based on configuration data
			groups = null;
			groups = new HashSet(Integer.parseInt(Data.config.getProperty(id.toString(), "50")));
	}

	public void write() throws IOException {
			// If record pointer is null: add record to the end of the file
			if (this.recordPointer == null) this.recordPointer = file.length();
			file.seek(this.recordPointer);

			writeString(this.id.toString(), 36);
			writeString(this.name, 100);
	}

	public String getName() {
		return (this.name);
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name;
	}
	
	@Override
	public int compareTo(Collection collection) {
		return (this.name.compareTo(collection.name));
	}
}
