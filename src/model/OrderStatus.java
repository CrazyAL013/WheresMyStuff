package model;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.UUID;

/**
 * Status Class Represents a single status
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 * 
 */
public class OrderStatus extends ObjectBase implements Comparable<OrderStatus> {
	private String description;

	public OrderStatus(RandomAccessFile file, Long recordPointer) {
		super(file, recordPointer);
	}

	public void read() throws IOException {
			file.seek(this.recordPointer);
			this.id = UUID.fromString(readString(36));
			this.description = readString(50);
	}

	public void write() throws IOException {
			// If record pointer is null: add record to the end of the file
			if (this.recordPointer == null) this.recordPointer = file.length();
			file.seek(this.recordPointer);

			writeString(this.id.toString(), 36);
			writeString(this.description, 50);
		}

	public String getDescription() {
		return (this.description);
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return this.description;
	}
	
	@Override
	public int compareTo(OrderStatus orderStatus) {
		return (this.description.compareTo(orderStatus.description));
	}
}
