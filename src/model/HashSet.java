package model;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.UUID;


/**
 * Stores an unordered collection of objects, using a hash table with
 * separate chaining to store a linked list in each bucket.
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 */
public class HashSet {
	private Node[] buckets;
	private int bucket;
	private int previousBucket;
	private Node current;
	private Node previous;
	private int size;

	public HashSet(int bucketsLength) {
		// Create the buckects array (add 30% to the number of buckets specified) 
		bucketsLength += (int)Math.ceil(bucketsLength * 0.3);
		buckets = new Node[(bucketsLength)];
		size = 0;
	}

	public boolean contains(UUID key) {
		return (get(key) != null);
	}
	
	public int count() {
		return (this.size);
	}
	
	public Object get(UUID key) {
		int hashCode = getHashCode(key.toString());

		Node current = buckets[hashCode];
		while (current != null) {
			if (current.key.equals(key.toString())) return current.data;
			current = current.next;
		}
		return null;
	}

	public void Clear() {
		for (int index = 0; index < buckets.length; index++) {
			if (this.buckets[index] != null) {
				buckets[index] = null;
			}
		}
	}
	
	public boolean add(UUID key, Object data) {
		int hashCode = getHashCode(key.toString());
		
		// Set current node to the bucket corresponding to the hash code
		Node current = buckets[hashCode];
		
		while (current != null) {
			// Check if key already exists
			if (this.contains(key)) return false;
			
			// Get next location in the linked list
			current = current.next;
		}
		
		// Add new element to linked list
		Node newNode = new Node();
		newNode.key = key.toString();
		newNode.data = data;
		newNode.next = buckets[hashCode];
		buckets[hashCode] = newNode;
		size++;
		return true;
	}

	public boolean remove(UUID key) {
		int hashCode = getHashCode(key.toString());

		Node current = buckets[hashCode];
		Node previous = null;
		while (current != null) {
			if (current.key.equals(key.toString())) {
				if (previous == null)
					buckets[hashCode] = current.next;
				else
					previous.next = current.next;
				size--;
				return true;
			}
			previous = current;
			current = current.next;
		}
		return false;
	}
	
	public Iterator iterator() {
		return new HashSetIterator();
	}

	public int size() {
		return size;
	}

	private int getHashCode(String key) {
		final int HASH_MULTIPLIER = 31;
		int hashCode = 0;
		for(int index = 0; index < key.length(); index++) {
			hashCode = HASH_MULTIPLIER * hashCode + key.charAt(index);
		}
		
		// Restrict hash code based on the number of buckets
		hashCode = hashCode % buckets.length;
		return Math.abs(hashCode);
	}
	
	public String getStatistics() {
		String result;
		int usedBuckets = 0;
		
		for (int index = 0; index < buckets.length; index++) {
			if (this.buckets[index] != null) {
				usedBuckets++;
			}
		}
		
		result = "Total buckets: " + this.buckets.length + "\r\n";
		result += "Total items: " + this.size + "\r\n";
		result += "Used buckets: " + usedBuckets + "\r\n";
		result += "Average items per bucket: " + ((this.size)/usedBuckets) + "\r\n";
		return (result);
	}
	
	/**
	 * Implements a (linked list) node for use within the hashset
	 * 
	 * @author Alex Tompkins, Sir Winston Churchill HS
	 */
	private class Node {
		public String key;
		public Object data;
		public Node next;
	}

	/**
	 * Implements an iterator for the hashset.
	 * 
	 * @author Alex Tompkins, Sir Winston Churchill HS
	 */
	private class HashSetIterator implements Iterator {
		public HashSetIterator() {
			current = null;
			bucket = -1;
			previous = null;
			previousBucket = -1;
		}

		public boolean hasNext() {
			if (current != null && current.next != null)
				return true;
			for (int b = bucket + 1; b < buckets.length; b++)
				if (buckets[b] != null)
					return true;
			return false;
		}

		public Object next() {
			previous = current;
			previousBucket = bucket;
			if (current == null || current.next == null) {
				// Move to next bucket
				bucket++;

				while (bucket < buckets.length && buckets[bucket] == null)
					bucket++;
				if (bucket < buckets.length)
					current = buckets[bucket];
				else
					throw new NoSuchElementException();
			} else
				// Move to next element in bucket
				current = current.next;
			return current.data;
		}

		public void remove() {
			if (previous != null && previous.next == current)
				previous.next = current.next;
			else if (previousBucket < bucket)
				buckets[bucket] = current.next;
			else
				throw new IllegalStateException();
			current = previous;
			bucket = previousBucket;
		}
	}
}