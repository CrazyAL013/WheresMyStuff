package model;

import global.Data;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.UUID;

/**
 * Item Class Represents a single stock item
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 * 
 */
public class Item extends ObjectBase {
	private String name;
	private int stockQuantity;
	private int reOrderLevel;
	private Collection collection;
	private Group group;
	private Category category;
	private String storageLocation;
	private String description;

	public Item(RandomAccessFile file, Long recordPointer) {
		super(file, recordPointer);
	}

	public void read() throws IOException {
			file.seek(this.recordPointer);
			this.id = UUID.fromString(readString(36));
			this.name = readString(150);
			this.stockQuantity = file.readInt();
			this.reOrderLevel = file.readInt();
			this.collection = (Collection) Data.collections.get(UUID.fromString(readString(36)));
			this.group = (Group) this.collection.groups.get(UUID.fromString(readString(36)));
			this.category = (Category) this.group.categories.get(UUID.fromString(readString(36)));
			this.storageLocation = readString(50);
			this.description = readString(500);
	}

	public void write() throws IOException {
			// If record pointer is null: add record to the end of the file
			if (this.recordPointer == null) this.recordPointer = file.length();
			file.seek(this.recordPointer);

			writeString(this.id.toString(), 36);
			writeString(this.name, 150);
			file.writeInt(this.stockQuantity);
			file.writeInt(this.reOrderLevel);
			writeString(this.collection.getId().toString(), 36);
			writeString(this.group.getId().toString(), 36);
			writeString(this.category.getId().toString(), 36);
			writeString(this.storageLocation, 50);
			writeString(this.description, 500);
	}
	
	
	/**
	 * Returns a string containing the object data in comma-delimited format
	 *
	 * @return: A String containing the data record
	 */
	public String getExportData() {
		StringBuilder data = new StringBuilder();
		
		data.append("\"" + this.id.toString() + "\",");
		data.append("\"" + this.name + "\",");
		data.append(this.stockQuantity + ",");
		data.append(this.reOrderLevel + ",");
		data.append("\"" + this.collection.getId().toString() + "\",");
		data.append("\"" + this.group.getId().toString() + "\",");
		data.append("\"" + this.category.getId().toString() + "\",");
		data.append("\"" + this.storageLocation + "\",");
		data.append("\"" + this.description + "\"");
		
		return (data.toString().replaceAll("\n", "\\\\n"));
	}

	/**
	 * Imports data into the object
	 *
	 * @param data: A comma-delimited string containing the data to import
	 */
	public void importData(String data) throws IOException {
		// Extract the individual fields from the string
        String[] fields = data.replace("\\n","\n").split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
        
        // Assign the data to each field in the object
    	this.id = (fields[0].replace("\"", "").length() != 0) ? UUID.fromString(fields[0].replace("\"", "")) : this.id;
		this.name = fields[1].replace("\"", "");
		this.stockQuantity = Integer.parseInt(fields[2].replace("\"", ""));
		this.reOrderLevel = Integer.parseInt(fields[3].replace("\"", ""));
		this.collection = (Collection) Data.collections.get(UUID.fromString(fields[4].replace("\"", "")));
		this.group = (Group) this.collection.groups.get(UUID.fromString(fields[5].replace("\"", "")));
		this.category = (Category) this.group.categories.get(UUID.fromString(fields[6].replace("\"", "")));
		this.storageLocation = fields[7].replace("\"", "");
		this.description = fields[8].replace("\"", "");
		
		// Write the data to file
		write();
	}

	public String getName() {
		return (this.name);
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStockQuantity() {
		return (this.stockQuantity);
	}

	public void setStockQuantity(int stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public int getReOrderLevel() {
		return (this.reOrderLevel);
	}

	public void setReOrderLevel(int reOrderLevel) {
		this.reOrderLevel = reOrderLevel;
	}

	public Collection getCollection() {
		return (this.collection);
	}

	public void setCollection(Collection collection) {
		this.collection = collection;
	}

	public Group getGroup() {
		return (this.group);
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public Category getCategory() {
		return (this.category);
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getStorageLocation() {
		return (this.storageLocation);
	}

	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}

	public String getDescription() {
		return (this.description);
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
