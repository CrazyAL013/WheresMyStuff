package model;

import global.Data;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.UUID;

/**
 * Group Class Represents a single group
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 * 
 */
public class Group extends ObjectBase implements Comparable<Group> {
	private String name;
	private UUID collectionID;
	public HashSet categories = new HashSet(100);

	public Group(RandomAccessFile file, Long recordPointer) {
		super(file, recordPointer);
	}

	public void read() throws IOException {
			file.seek(this.recordPointer);
			this.id = UUID.fromString(readString(36));
			this.name = readString(100);
			this.collectionID = UUID.fromString(readString(36));

			// Resize the categories hashset based on configuration data
			categories = null;
			categories = new HashSet(Integer.parseInt(Data.config.getProperty(id.toString(), "100")));
	}

	public void write() throws IOException {
			// If record pointer is null: add record to the end of the file
			if (this.recordPointer == null) this.recordPointer = file.length();
			file.seek(this.recordPointer);

			writeString(this.id.toString(), 36);
			writeString(this.name, 100);
			writeString(this.collectionID.toString(), 36);
	}

	public String getName() {
		return (this.name);
	}

	public void setName(String name) {
		this.name = name;
	}

	public UUID getCollectionID() {
		return (this.collectionID);
	}

	public void setCollectionID(UUID collectionID) {
		this.collectionID = collectionID;
	}

	@Override
	public String toString() {
		return this.name;
	}
	
	@Override
	public int compareTo(Group group) {
		return (this.name.compareTo(group.name));
	}

}
