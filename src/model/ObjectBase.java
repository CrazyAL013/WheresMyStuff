package model;
import global.Data;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.ParseException;
import java.util.UUID;
import static global.Constants.*;

/**
 * ObjectBase Class
 * An abstract superclass that provides common functionality for other data classes
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 *
 */
public abstract class ObjectBase {
	protected UUID id;
	protected Long recordPointer = null;
	protected RandomAccessFile file = null;
	
	public ObjectBase() {}

	public ObjectBase(RandomAccessFile file, Long recordPointer) {
		this.id = UUID.randomUUID();
		this.recordPointer = recordPointer;
		this.file = file;
	}
	
	public UUID getId() {
		return this.id;
	}
	
	public Long getRecordPointer() {
		return this.recordPointer;
	}
	
	public void remove() throws IOException {
		this.id = UUID_EMPTY;
		write();
	}
	
	protected void setId(UUID id) {
		this.id = id;
	}
	
	/**
	 * Generic function to write a String to a file
	 * @param data: The String to be written
	 * @param length: The fixed length for the record
	 */
	protected void writeString(String data, int length) throws IOException {	
		StringBuffer buffer = new StringBuffer(data);
		buffer.setLength(length);
		this.file.writeBytes(buffer.toString());
	}
	
	/**
	 * Generic function to read a String from a file
	 * @param length: The fixed length for the record
	 * @return
	 */
	protected String readString(int length) throws IOException {
		StringBuffer buffer = new StringBuffer(length);
		char character;
		for (int count = 0; count < length; count++) {
			character = (char)this.file.readByte();
			// Strings in a record are fixed length: ignore padding characters (nulls)
			if (character != '\0') buffer.append(character);
		}
		return (buffer.toString());
	}

	
	/**
	 * Abstract methods that must be implemented by inheritors
	 */
	abstract public void read() throws IOException, ParseException;
	abstract public void write() throws IOException;
}
