package model;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.UUID;

/**
 * Vendor Class Represents a single vendor
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 * 
 */
public class Vendor extends ObjectBase implements Comparable<Vendor> {
	private String name;
	private String website;

	public Vendor(RandomAccessFile file, Long recordPointer) {
		super(file, recordPointer);
	}

	public void read() throws IOException {
			file.seek(this.recordPointer);
			this.id = UUID.fromString(readString(36));
			this.name = readString(100);
			this.website = readString(150);
	}

	public void write() throws IOException {
			// If record pointer is null: add record to the end of the file
			if (this.recordPointer == null) this.recordPointer = file.length();
			file.seek(this.recordPointer);

			writeString(this.id.toString(), 36);
			writeString(this.name, 100);
			writeString(this.website, 150);
	}

	public String getName() {
		return (this.name);
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getWebsite() {
		return (this.website);
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public int compareTo(Vendor vendor) {
		return (this.name.compareTo(vendor.name));
	}
}
