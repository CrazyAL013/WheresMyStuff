package application;

import org.eclipse.swt.widgets.Display;

import forms.MainForm;
import forms.SplashForm;

/**
 * Where's My Stuff? Stuff Management for Humans v1.0. 
 * An inventory management application.
 * 
 * Built March 2012 using the Eclipse IDE (Galileo) on an Ubuntu 11.04 x64 PC
 * 
 * @author Alex Tompkins, Sir Winston Churchill HS
 * 
 */
public class Program {

	public static void main(String[] args) {
		Display.setAppName("Where's My Stuff?");

		// Display the splash form (while the data is loaded)
		SplashForm splashForm = new SplashForm();

		// Display the main form
		MainForm mainForm = new MainForm(splashForm);
	}

}
