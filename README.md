Wheres My Stuff
===
![App Logo](https://www.demongeek.com/images/icons/demongeek-icon-64.png)

**Wheres My Stuff** is an inventory management application that focuses on maintaining an inventory of parts (stock) and maintaining a history of orders made by the user. It was written in Java using SWT (Standard Widget Toolkit) for my grade 12 IB Computer Science class. 

For a full writeup on this project see the [PDF Dossier.](https://gitlab.com/CrazyAL013/WheresMyStuff/tree/master/docs)


---

* [Main Functions](#main-functions)
* [Data Storage](#data-storage)

---

## Main Functions
* Add stock item
* Modify stock item
* Remove stock item
* Browse stock
* Search stock
* Create order
* Modify order
* Delete order
* Browse orders
* Search orders
* Update stock

## Data Storage
During normal operation, the application will use data structures to manipulate data in memory but in order to retain data between sessions, the application will store all its data in files on disk. At start-up, for best performance, the application will load all data from disk files into memory-based data structures. As data in the data structures is added/modified/removed the application will use random (direct) file access to update individual records in the disk file to ensure it is always up-to-date.
The data will be divided into multiple files; stock, orders, order items, collections, groups, categories, vendors and order status.

The application user will not be aware of the disk files and will not be required to 'open' or 'save' data since the application will automatically manage those operations itself.
```